import js from '@eslint/js';
import react from 'eslint-plugin-react';
import reactHooks from 'eslint-plugin-react-hooks';
import reactRefresh from 'eslint-plugin-react-refresh';
import tssUnusedClasses from 'eslint-plugin-tss-unused-classes';
import globals from 'globals';
import tseslint from 'typescript-eslint';

export default tseslint.config({
  extends: [js.configs.recommended, ...tseslint.configs.recommendedTypeChecked],
  files: ['**/*.{ts,tsx}'],
  ignores: ['dist'],
  languageOptions: {
    ecmaVersion: 2020,
    globals: globals.browser,
    parserOptions: {
      project: [
        './tsconfig.node.json',
        './tsconfig.app.json'
      ],
      tsconfigRootDir: import.meta.dirname
    }
  },
  settings: {
    react: {
      version: 'detect'
    }
  },
  plugins: {
    react,
    'react-hooks': reactHooks,
    'react-refresh': reactRefresh,
    'tss-unused-classes': tssUnusedClasses
  },
  rules: {
    ...react.configs.recommended.rules,
    ...react.configs['jsx-runtime'].rules,
    ...reactHooks.configs.recommended.rules,
    'react-refresh/only-export-components': [
      'warn',
      {allowConstantExport: true}
    ],
    'tss-unused-classes/unused-classes': 'warn',
    quotes: ['warn', 'single'],
    semi: ['warn', 'always'],
    'react/no-unescaped-entities': 'off'
  }
});
