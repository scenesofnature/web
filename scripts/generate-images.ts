import {$} from 'bun';
import path from 'path';
import {exit} from 'process';
import shellScript from './docker/generate-images.sh?url';

const promptInput = prompt('Start generating images? This can take a LOT of time... (y/n)');

if (!promptInput || !['y', 'yes'].includes(promptInput.toLowerCase())) {
  await $`echo "aborting..."`;
  exit(0);
}

const image = 'dpokidov/imagemagick:latest-bullseye';

const input = path.resolve(__dirname, '../photos');
const output = path.resolve(__dirname, '../src/assets/photos/generated');
const scripts = path.resolve(__dirname, 'docker');
const shellScriptName = path.basename(shellScript);

await $`docker run --rm --entrypoint /bin/bash -v "${input}:/input" -v "${output}:/output" -v "${scripts}:/scripts" ${image} /scripts/${shellScriptName}`;
