import {$} from 'bun';

await $`rm -rf 3rd-party`;
await $`license-checker-rseidelsohn --excludePrivatePackages --markdown --files ./3rd-party/licences --out ./3rd-party/libraries.md`;
