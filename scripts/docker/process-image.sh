#!/bin/bash

INFILE="$1"
INPUT_DIR="$2"
OUTPUT_DIR="$3"

QUALITY=70
SIZES=(3840 2560 1920 1440 1366 1024 768 720 640 480)
FORMATS=('avif' 'webp')

# replace input directory with output directory in the file path
outfile_base="${INFILE/#$INPUT_DIR/$OUTPUT_DIR}"
# remove .avif extension
outfile_base="${outfile_base%.avif}"

# create the output directory if it doesn't exist
mkdir -p "$(dirname "$outfile_base")"

for format in "${FORMATS[@]}"; do
  for size in "${SIZES[@]}"; do
    # define the filename based on size and format
    outfile="${outfile_base}-${size}.${format}"

    magick "$INFILE" -quality $QUALITY -resize "${size}x>" "$format:$outfile"
  done
done
