#!/bin/bash

INPUT_DIR="/input"
OUTPUT_DIR="/output"
SCRIPTS_DIR="/scripts"
NUMBER_OF_CORES=$(nproc)

if [ "$GITLAB_CI" == "true" ]; then
  INPUT_DIR="photos"
  OUTPUT_DIR="src/assets/photos/generated"
  SCRIPTS_DIR="scripts/docker"
fi

# clear the output directory before starting the conversion
rm -rf "${OUTPUT_DIR:?}"/*

echo "starting image creation"
echo "this may take a while"
echo "..."
SECONDS=0

# find all AVIF files and create size variations of them using xargs parallel
find "$INPUT_DIR" -name "*.avif" -print0 | xargs -0 -I {} -P "$NUMBER_OF_CORES" "$SCRIPTS_DIR/process-image.sh" {} "$INPUT_DIR" "$OUTPUT_DIR"

duration=$SECONDS
echo "image creation finished"
echo "duration: ${duration}s"

# change owner of generated resources to the owner of the host machine
chown -R "$(stat -c '%u:%g' /scripts)" "$OUTPUT_DIR"
