user-agent: *
disallow: /*.webp

sitemap: https://www.scenesofnature.earth/sitemap.xml
