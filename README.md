[![Latest Release](https://gitlab.com/scenesofnature/web/-/badges/release.svg)](https://gitlab.com/scenesofnature/web/-/releases)
[![pipeline status](https://gitlab.com/scenesofnature/web/badges/main/pipeline.svg)](https://gitlab.com/scenesofnature/web/-/commits/main)

# Scenes of Nature

[Scenes of Nature](https://scenesofnature.earth) is the portfolio website of Simon Jadwiczek.\
This repository contains the source code, all used photographs are located in the [photos](photos) submodule.

## Licence

### This project

The source code in this repository, with the exception of all submodules, is licenced under the [MIT licence](LICENCE).

### 3rd party libraries

This project uses the libraries listed in [libraries.md](3rd-party/libraries.md). See [licences](3rd-party/licences) for the used licences.

## Development

### Prerequisites

- [Git LFS](https://git-lfs.com)
- [Bun](https://bun.sh/docs/installation)

### Local development

1. clone the repository and its submodules
   ```
   git lfs install
   git clone --recurse-submodules git@gitlab.com:scenesofnature/web.git
   ```
2. install dependencies
   ```
   bun install
   ```
3. create responsive images from images in [photos](photos) submodule by one of the following options:
   - manually generate images _(requires [Docker](https://docs.docker.com/engine/install), may take more than thirty minutes to complete)_
     ```
     bun generate:images
     ```
   - download generated images from CI `generate_images` job artefact _(only version of branch `main` available)_
     ```
     wget "https://gitlab.com/scenesofnature/web/-/jobs/artifacts/main/download?job=generate_images" -O generated-images.zip
     unzip -o generated-images.zip
     rm generated-images.zip
     ```
4. start application
   ```
   bun dev
   ```
