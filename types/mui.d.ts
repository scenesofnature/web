// for module augmentation to work, at least one import statement is required
// import '@mui/material';
import {CSSProperties} from 'react';

declare module '@mui/material/styles' {
  interface TypographyVariants {
    code1: CSSProperties;
    code2: CSSProperties;
  }

  // allow configuration using `createTheme`
  interface TypographyVariantsOptions {
    code1?: CSSProperties;
    code2?: CSSProperties;
  }
}

// Update the Typography's variant prop options
declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    code1: true;
    code2: true;
  }
}
