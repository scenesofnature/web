import {Box, Typography, useMediaQuery, useTheme} from '@mui/material';
import LogoBlack from '../../assets/logo-black.svg?react';
import LogoWhite from '../../assets/logo-white.svg?react';

type LogoProps = {
  readonly color: 'white' | 'black';
  readonly onClick?: () => void;
};

const Logo = (props: LogoProps) => {
  const theme = useTheme();
  const isLargerThanMobile = useMediaQuery(theme.breakpoints.up('sm'));
  const isClickable = props.onClick !== undefined;

  return (
    <Box width='100%' display='flex' alignItems='center' mt='2vh'>
      <Box flex={1} display='flex' justifyContent='flex-end'>
        <Typography variant='h2' color={`common.${props.color}`} sx={isClickable ? {cursor: 'pointer'} : undefined} onClick={props.onClick}>
          S. Jadwiczek
        </Typography>
      </Box>
      <Box width={isLargerThanMobile ? 60 : 40} height={isLargerThanMobile ? 60 : 40}
           sx={isClickable ? {cursor: 'pointer'} : undefined}
           onClick={props.onClick}>
        {props.color === 'white'
          ? <LogoWhite/>
          : <LogoBlack/>
        }
      </Box>
      <Box flex={1} display='flex' justifyContent='flex-start'>
        <Typography variant='h2'
                    color={`common.${props.color}`}
                    pl={isLargerThanMobile ? 2 : 1}
                    sx={isClickable ? {cursor: 'pointer'} : undefined}
                    onClick={props.onClick}>
          Photography
        </Typography>
      </Box>
    </Box>
  );
};

export default Logo;
