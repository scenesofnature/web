import {getAvifSrcSet, getWebpSrcSet, Image} from '../../utils/image.ts';

type ResponsivePictureProps = {
  readonly image: Image;
  readonly sizes: string;
  readonly srcFallback: string;
  readonly className?: string;
}

const ResponsivePicture = (props: ResponsivePictureProps) => (
  <picture>
    <source type='image/avif' srcSet={getAvifSrcSet(props.image.urls)} sizes={props.sizes}/>
    <source type='image/webp' srcSet={getWebpSrcSet(props.image.urls)} sizes={props.sizes}/>
    <img src={props.srcFallback} alt={props.image.description} className={props.className}/>
  </picture>
);

export default ResponsivePicture;
