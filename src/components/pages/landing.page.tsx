import SouthOutlinedIcon from '@mui/icons-material/SouthOutlined';
import {Box, Typography, useMediaQuery, useTheme} from '@mui/material';
import {tss} from 'tss-react/mui';
import {landingImage} from '../../assets/photos/general.ts';
import {routes} from '../../bootstrapping/navigation/routes.ts';
import {useMouseWheelListener} from '../../utils/hooks/use-mouse-wheel-listener.ts';
import {useSwipeListener} from '../../utils/hooks/use-swipe-listener.ts';
import {PageProps} from '../../utils/props.ts';
import Logo from '../shared/logo.tsx';
import ResponsivePicture from '../shared/responsive-picture.tsx';

const useStyles = tss.create({
  backgroundPicture: {
    height: '100%',
    width: '100%',
    objectFit: 'cover'
  }
});

const LandingPage = (props: PageProps) => {
  const theme = useTheme();
  const isLargerThanMobile = useMediaQuery(theme.breakpoints.up('sm'));
  const {classes} = useStyles();

  useMouseWheelListener({downListener: () => props.handleNavigation(routes.overview, 'mouse-wheel')}, props.isCurrentPage);
  useSwipeListener({upListener: () => props.handleNavigation(routes.overview, 'swipe')}, props.isCurrentPage);

  return (
    <Box position='relative' height='100%' width='100%'>
      <ResponsivePicture image={landingImage} sizes='300vw' srcFallback={landingImage.urls.webp_1920}
                         className={classes.backgroundPicture}/>
      <Box position='absolute' top={0} bottom={0} left={0} right={0}
           display='flex' flexDirection='column' justifyContent='space-between' alignItems='center'>
        <Box width='100%' flex={1}>
          <Logo color='white'/>
        </Box>
        <Typography variant='h1' color='common.white' mb={isLargerThanMobile ? '10vh' : '7vh'}>Scenes of Nature</Typography>
        <Box flex={1} alignContent='end'>
          <Box mb='16vh'>
            <Box component='button' tabIndex={-1} aria-label='scroll to overview'
                 width={isLargerThanMobile ? 60 : 40} height={isLargerThanMobile ? 60 : 40}
                 display='flex' justifyContent='center' alignItems='center'
                 color='common.white'
                 border={isLargerThanMobile ? 2.8 : 2}
                 sx={{transform: 'rotate(45deg)', cursor: 'pointer', background: 'none'}}
                 onClick={() => props.handleNavigation(routes.overview, 'button')}>
              <SouthOutlinedIcon fontSize={isLargerThanMobile ? 'large' : 'medium'} sx={{transform: 'rotate(-45deg)'}}/>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default LandingPage;
