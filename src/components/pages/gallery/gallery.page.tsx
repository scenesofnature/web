import {Box} from '@mui/material';
import {useEffect, useState} from 'react';
import {canadaImages} from '../../../assets/photos/001-canada.ts';
import {australiaImages} from '../../../assets/photos/002-australia.ts';
import {newZealandImages} from '../../../assets/photos/003-new-zealand.ts';
import {nepalImages} from '../../../assets/photos/004-nepal.ts';
import {botswanaImages} from '../../../assets/photos/005-botswana.ts';
import {birdImages} from '../../../assets/photos/006-birds.ts';
import {Chapter, chapterQueries, getCurrentChapter} from '../../../bootstrapping/navigation/queries.ts';
import {routes} from '../../../bootstrapping/navigation/routes.ts';
import {useGlobalStyles} from '../../../bootstrapping/theming/global-styles.ts';
import {PageProps} from '../../../utils/props.ts';
import GalleryImages from './gallery-images.tsx';

const getImages = (chapter: Chapter) => {
  switch (chapter) {
    case chapterQueries.birds:
      return birdImages;
    case chapterQueries.botswana:
      return botswanaImages;
    case chapterQueries.nepal:
      return nepalImages;
    case chapterQueries.newZealand:
      return newZealandImages;
    case chapterQueries.australia:
      return australiaImages;
    case chapterQueries.canada:
      return canadaImages;
  }
};

const GalleryPage = (props: PageProps) => {
  const {handleNavigation} = props;
  const {classes: globalClasses} = useGlobalStyles();
  const [chapter, setChapter] = useState<Chapter | null>(null);

  // set current chapter
  useEffect(() => {
    if (props.isCurrentPage) {
      const currentChapter = getCurrentChapter();

      if (currentChapter === null) {
        handleNavigation(routes.overview, 'button', undefined, true);
      }

      setChapter(currentChapter);
    } else {
      setChapter(null);
    }
  }, [props.isCurrentPage, handleNavigation]);

  return (
    <Box height='100%' width='100%' minWidth='100%' className={globalClasses.themeSwitched}>
      {chapter &&
        <GalleryImages images={getImages(chapter)} onClose={() => handleNavigation(routes.overview, 'button')}/>
      }
    </Box>
  );
};

export default GalleryPage;
