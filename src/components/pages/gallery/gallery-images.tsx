import {IsoOutline} from '@carbon/icons-react';
import ArrowBackIosNewOutlinedIcon from '@mui/icons-material/ArrowBackIosNewOutlined';
import ArrowForwardIosOutlinedIcon from '@mui/icons-material/ArrowForwardIosOutlined';
import CameraOutlinedIcon from '@mui/icons-material/CameraOutlined';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import ShutterSpeedOutlinedIcon from '@mui/icons-material/ShutterSpeedOutlined';
import {Box, IconButton, Typography} from '@mui/material';
import {useState} from 'react';
import {tss} from 'tss-react/mui';
import {useArrowKeyListener} from '../../../utils/hooks/use-arrow-key-listener.ts';
import {useEscapeKeyListener} from '../../../utils/hooks/use-escape-key-listener.ts';
import {useIsLandscapeOrientation} from '../../../utils/hooks/use-is-landscape-orientation.ts';
import {useSwipeListener} from '../../../utils/hooks/use-swipe-listener.ts';
import {Image} from '../../../utils/image.ts';
import {ZERO_WIDTH_SPACE} from '../../../utils/special-characters.ts';
import ResponsivePicture from '../../shared/responsive-picture.tsx';

type GalleryImageProps = {
  readonly images: Image[];
  readonly onClose: () => void;
};

const useStyles = tss.create({
  image: {
    height: '100%',
    width: '100%',
    objectFit: 'contain',
    transition: 'transform 0.3s ease',
    willChange: 'transform'
  },
  previous: {
    transform: 'translateX(-100vw)'
  },
  next: {
    transform: 'translateX(100vw)'
  }
});

const GalleryImages = (props: GalleryImageProps) => {
  const {classes, cx} = useStyles();
  const [currentImage, setCurrentImage] = useState(0);
  const isLandscape = useIsLandscapeOrientation();

  const isFirstImage = () => currentImage === 0;
  const isLastImage = () => currentImage === props.images.length - 1;

  const toPreviousImage = () => {
    if (!isFirstImage()) {
      setCurrentImage(prev => prev - 1);
    }
  };

  const toNextImage = () => {
    if (!isLastImage()) {
      setCurrentImage(prev => prev + 1);
    }
  };

  useSwipeListener({
    rightListener: toPreviousImage,
    leftListener: toNextImage
  });

  useArrowKeyListener({
    rightListener: toNextImage,
    leftListener: toPreviousImage
  });

  useEscapeKeyListener(props.onClose);

  return (
    <Box height='100%' width='100%' py='2vh' display='flex' flexDirection='column' justifyContent='space-between'>
      <Box display='flex' justifyContent='flex-end' mb='2vh' pr='2vw'>
        <IconButton aria-label='back to overview' onClick={props.onClose}>
          <CloseOutlinedIcon/>
        </IconButton>
      </Box>
      <Box position='relative' flex={1} display='flex' justifyContent='space-between' alignItems='center' px='2vw'>
        {props.images.map((_, i) =>
          <Box key={i} position='absolute' top={0} bottom={0} left={0} right={0}>
            <ResponsivePicture image={props.images[i]} sizes='100vw' srcFallback={props.images[i].urls.webp_1024}
                               className={cx(classes.image, i < currentImage && classes.previous, i > currentImage && classes.next)}/>
          </Box>
        ).filter((_, i) =>
          // only render previous, current and next image for better performance
          i === currentImage - 1 || i === currentImage || i === currentImage + 1
        )}
        <IconButton disabled={isFirstImage()} aria-label='previous image' onClick={() => setCurrentImage(prev => prev - 1)}>
          <ArrowBackIosNewOutlinedIcon/>
        </IconButton>
        <IconButton disabled={isLastImage()} aria-label='next image' onClick={() => setCurrentImage(prev => prev + 1)}>
          <ArrowForwardIosOutlinedIcon/>
        </IconButton>
      </Box>
      <Box display='flex' flexDirection='column' alignItems='flex-end' mt={isLandscape ? '2vh' : 0} px='2vw'>
        <Typography variant='caption' width='100%' minHeight={isLandscape ? undefined : 45} px='5vw'>
          {props.images[currentImage].description}
        </Typography>
        {!props.images[currentImage].lens &&
          <Typography variant='code2'>{ZERO_WIDTH_SPACE}</Typography>
        }
        <Typography variant='code2' mt={isLandscape ? '2vh' : '10vh'} sx={{userSelect: 'text'}}>
          {props.images[currentImage].camera}
        </Typography>
        {props.images[currentImage].lens &&
          <Typography variant='code2' sx={{userSelect: 'text'}}>{props.images[currentImage].lens}</Typography>
        }
        <Box display='flex' alignItems='center' mt={1}>
          <CameraOutlinedIcon fontSize='small'/>
          <Typography variant='code2' ml={1} mr={3}>{props.images[currentImage].aperture}</Typography>
          <ShutterSpeedOutlinedIcon fontSize='small'/>
          <Typography variant='code2' ml={1} mr={3}>{props.images[currentImage].shutterSpeed}</Typography>
          <IsoOutline size={20}/>
          <Typography variant='code2' ml={1}>{props.images[currentImage].iso}</Typography>
        </Box>
      </Box>
    </Box>
  );
};

export default GalleryImages;
