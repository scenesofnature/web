import {Box, Typography} from '@mui/material';
import {portraitImage, portraitLandscapeImage} from '../../../assets/photos/general.ts';
import BlurredBackgroundImage from './blurred-background-image.tsx';

type WhoAmIProps = {
  readonly isLandscape: boolean;
  readonly onClick: () => void;
};

const WhoAmI = (props: WhoAmIProps) => (
  <Box height='100%' display='flex' flexDirection='column'>
    <Typography variant='h3' mb='3vh'>Who am I?</Typography>
    <Box flex={1} sx={{cursor: 'pointer'}} onClick={props.onClick}>
      <BlurredBackgroundImage image={props.isLandscape ? portraitImage : portraitLandscapeImage}/>
    </Box>
  </Box>
);

export default WhoAmI;
