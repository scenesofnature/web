import {Box, Divider, Typography} from '@mui/material';
import {useEffect, useState} from 'react';
import {FooterDialogue, getCurrentFooterDialogue, queries} from '../../../bootstrapping/navigation/queries.ts';
import {routes} from '../../../bootstrapping/navigation/routes.ts';
import {useIsLandscapeOrientation} from '../../../utils/hooks/use-is-landscape-orientation.ts';
import {useLocationListener} from '../../../utils/hooks/use-location-listener.ts';
import {useMouseWheelListener} from '../../../utils/hooks/use-mouse-wheel-listener.ts';
import {useSwipeListener} from '../../../utils/hooks/use-swipe-listener.ts';
import {PageProps} from '../../../utils/props.ts';
import {ZERO_WIDTH_SPACE} from '../../../utils/special-characters.ts';
import Logo from '../../shared/logo.tsx';
import Chapters from './chapters.tsx';
import Footer from './footer/footer.tsx';
import WhoAmI from './who-am-i.tsx';

const OverviewPage = (props: PageProps) => {
  const {handleNavigation} = props;
  const isLandscape = useIsLandscapeOrientation();
  const [footerDialogue, setFooterDialogue] = useState<FooterDialogue | null>(null);

  useMouseWheelListener({
    upListener: () => handleNavigation(routes.landing, 'mouse-wheel'),
    leftListener: () => handleNavigation(routes.about, 'mouse-wheel')
  }, props.isCurrentPage && footerDialogue === null);

  useSwipeListener({
    downListener: () => handleNavigation(routes.landing, 'swipe'),
    rightListener: () => handleNavigation(routes.about, 'swipe')
  }, props.isCurrentPage && footerDialogue === null);

  // set current footer dialogue
  useEffect(() => {
    if (props.isCurrentPage) {
      const currentFooterDialogue = getCurrentFooterDialogue();
      setFooterDialogue(currentFooterDialogue);
    } else {
      setFooterDialogue(null);
    }
  }, [props.isCurrentPage, handleNavigation]);

  // set current footer dialogue on browser navigation
  useLocationListener(() => {
    setFooterDialogue(getCurrentFooterDialogue());
  });

  return (
    <Box height='100%' width='100%' minWidth='100%'
         display='flex' flexDirection='column' alignItems='center' justifyContent='space-between'>
      <Logo color='black' onClick={() => handleNavigation(routes.landing, 'button')}/>
      <Box flex={1} width='100%' display='flex' flexDirection={isLandscape ? 'row' : 'column-reverse'}
           py='3vh' px={isLandscape ? '2vw' : undefined}>
        <Box height={isLandscape ? '100%' : '40%'} width={isLandscape ? '30%' : '100%'}>
          <WhoAmI isLandscape={isLandscape} onClick={() => handleNavigation(routes.about, 'button')}/>
        </Box>
        {isLandscape &&
          <Box display='flex' flexDirection='column'>
            <Typography variant='h3' mb='3vh'>{ZERO_WIDTH_SPACE}</Typography>
            <Divider orientation='vertical' sx={{mx: '2vw', flex: 1}}/>
          </Box>
        }
        <Box flex={1} mb={isLandscape ? undefined : '4vh'}>
          <Chapters onClick={(chapter) => handleNavigation(routes.gallery, 'button', new URLSearchParams([[queries.chapter, chapter]]))}/>
        </Box>
      </Box>
      <Footer dialogue={footerDialogue}
              setDialogue={(dialogue) => {
                setFooterDialogue(dialogue);
                props.handleNavigation(
                  routes.overview,
                  'button',
                  dialogue === null ? undefined : new URLSearchParams([[queries.footerDialogue, dialogue]]));
              }}/>
    </Box>
  );
};

export default OverviewPage;
