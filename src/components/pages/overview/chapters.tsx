import {Box, Typography} from '@mui/material';
import {useRef} from 'react';
import {canadaImages} from '../../../assets/photos/001-canada.ts';
import {australiaImages} from '../../../assets/photos/002-australia.ts';
import {newZealandImages} from '../../../assets/photos/003-new-zealand.ts';
import {nepalImages} from '../../../assets/photos/004-nepal.ts';
import {botswanaImages} from '../../../assets/photos/005-botswana.ts';
import {birdImages} from '../../../assets/photos/006-birds.ts';
import {Chapter, chapterQueries} from '../../../bootstrapping/navigation/queries.ts';
import {useAspectRatioComparison} from '../../../utils/hooks/use-aspect-ratio-comparison.ts';
import ChapterTile from './chapter-tile.tsx';

type ChaptersProps = {
  readonly onClick: (chapter: Chapter) => void;
};

const birdImage = birdImages[16];
const botswanaImage = botswanaImages[10];
const nepalImage = nepalImages[3];
const newZealandImage = newZealandImages[3];
const australiaImage = australiaImages[3];
const canadaImage = canadaImages[10];

// Aspect ratio, at which the grid layout should be switched from 3 columns to 2 columns.
// In order for the grid items to have an aspect ratio as close to 3:2 as possible, layout should be switched at 18:3 aspect ratio.
// In order for the grid items to have at little "extra surface" in addition to 3:2 inner items, layout should be switched at 3:2 aspect ratio.
// However, the layout looks best if it is switched even later, so fck maths.
const CONTENT_COLUMN_SWITCH_ASPECT_RATIO = 7 / 4;

const Chapters = (props: ChaptersProps) => {
  const contentRef = useRef<HTMLDivElement>(null);
  const isContent3Columns = useAspectRatioComparison(contentRef, '>', CONTENT_COLUMN_SWITCH_ASPECT_RATIO);

  return (
    <Box height='100%' display='flex' flexDirection='column'>
      <Typography variant='h3' mb='3vh'>Chapters</Typography>
      <Box ref={contentRef}
           flex={1}
           display='grid'
           gridTemplateColumns={isContent3Columns ? '1fr 1fr 1fr' : '1fr 1fr'}
           gridTemplateRows={isContent3Columns ? '1fr 1fr' : '1fr 1fr 1fr'}>
        <ChapterTile title='Birds' image={birdImage} moveImageY={-7} onClick={() => props.onClick(chapterQueries.birds)}/>
        <ChapterTile title='Botswana' image={botswanaImage} moveImageY={-3} onClick={() => props.onClick(chapterQueries.botswana)}/>
        <ChapterTile title='Nepal' image={nepalImage} moveImageX={5} moveImageY={8} onClick={() => props.onClick(chapterQueries.nepal)}/>
        <ChapterTile title='New Zealand' image={newZealandImage} moveImageX={-4} moveImageY={5} onClick={() => props.onClick(chapterQueries.newZealand)}/>
        <ChapterTile title='Australia' image={australiaImage} moveImageX={-8} moveImageY={3} onClick={() => props.onClick(chapterQueries.australia)}/>
        <ChapterTile title='Canada' image={canadaImage} moveImageX={2} moveImageY={6} onClick={() => props.onClick(chapterQueries.canada)}/>
      </Box>
    </Box>
  );
};

export default Chapters;
