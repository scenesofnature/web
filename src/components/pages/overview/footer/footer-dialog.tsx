import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import {Dialog, DialogContent, DialogTitle, IconButton, Slide} from '@mui/material';
import {TransitionProps} from '@mui/material/transitions';
import {forwardRef, ReactElement} from 'react';
import {FooterDialogue, footerDialogueQueries} from '../../../../bootstrapping/navigation/queries.ts';
import Contact from './contact.tsx';
import Faq from './faq.tsx';
import Legal from './legal.tsx';

type FooterDialogProps = {
  readonly dialogue: FooterDialogue | null;
  readonly onClose: () => void;
};

const Transition = forwardRef<unknown, TransitionProps & { children: ReactElement }>((props, ref) => (
  // eslint-disable-next-line react/prop-types
  <Slide direction='up' ref={ref} {...props}>{props.children}</Slide>
));

Transition.displayName = 'Transition';

const FooterDialog = (props: FooterDialogProps) => (
  <Dialog open={props.dialogue !== null} onClose={props.onClose} keepMounted TransitionComponent={Transition}>
    <DialogTitle variant='subtitle1' display='flex' justifyContent='space-between' alignItems='center'>
      {(() => {
        switch (props.dialogue) {
          case footerDialogueQueries.faq:
            return 'FAQ';
          case footerDialogueQueries.contact:
            return 'Contact';
          case footerDialogueQueries.legal:
            return 'Legal';
          default:
            return <></>;
        }
      })()}
      <IconButton edge='end' sx={{ml: 20}} aria-label='close' onClick={props.onClose}>
        <CloseOutlinedIcon/>
      </IconButton>
    </DialogTitle>
    <DialogContent>
      {(() => {
        switch (props.dialogue) {
          case footerDialogueQueries.faq:
            return <Faq/>;
          case footerDialogueQueries.contact:
            return <Contact/>;
          case footerDialogueQueries.legal:
            return <Legal/>;
          default:
            return <></>;
        }
      })()}
    </DialogContent>
  </Dialog>
);

export default FooterDialog;
