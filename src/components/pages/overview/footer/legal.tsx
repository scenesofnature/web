import {DialogContentText, Link} from '@mui/material';
import {version} from '../../../../utils/env-variables.ts';

const THIRD_PARTY_LIBRARIES_FILE = 'https://gitlab.com/scenesofnature/web/-/blob/main/3rd-party/libraries.md';

const Legal = () => (
  <>
    <DialogContentText variant='code1'>website version: {version}</DialogContentText>
    <DialogContentText variant='subtitle2' color='inherit' mt={4} mb={1}>Privacy Policy</DialogContentText>
    <DialogContentText variant='code1'>No data is processed.</DialogContentText>
    <DialogContentText variant='subtitle2' color='inherit' mt={4} mb={1}>Libraries</DialogContentText>
    <DialogContentText variant='code1'>
      {'This site was created using the dependencies listed '}
      <Link href={THIRD_PARTY_LIBRARIES_FILE} target='_blank' rel='noreferrer'>here</Link>
      .
    </DialogContentText>
  </>
);

export default Legal;
