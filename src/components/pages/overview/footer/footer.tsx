import {Box, Typography} from '@mui/material';
import {FooterDialogue} from '../../../../bootstrapping/navigation/queries.ts';
import {BULLET} from '../../../../utils/special-characters.ts';
import FooterButton from './footer-button.tsx';
import FooterDialog from './footer-dialog.tsx';

type FooterProps = {
  readonly dialogue: FooterDialogue | null;
  readonly setDialogue: (dialogue: FooterDialogue | null) => void;
};

const Footer = (props: FooterProps) => (
  <Box display='flex' alignItems='center' mb='2vh'>
    <FooterButton text='FAQ' onClick={() => props.setDialogue('faq')}/>
    <Typography variant='button' mx={1} marginTop='-0.1875em'>{BULLET}</Typography>
    <FooterButton text='Contact' onClick={() => props.setDialogue('contact')}/>
    <Typography variant='button' mx={1} marginTop='-0.1875em'>{BULLET}</Typography>
    <FooterButton text='Legal' onClick={() => props.setDialogue('legal')}/>
    <FooterDialog dialogue={props.dialogue} onClose={() => props.setDialogue(null)}/>
  </Box>
);

export default Footer;
