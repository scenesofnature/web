import pgpKey from '/simon.jadwiczek@scenesofnature.earth.asc?url';
import ContentCopyOutlinedIcon from '@mui/icons-material/ContentCopyOutlined';
import DoneAllOutlinedIcon from '@mui/icons-material/DoneAllOutlined';
import {Button, DialogContentText, Link, Typography} from '@mui/material';
import {useState} from 'react';
import {ZERO_WIDTH_SPACE} from '../../../../utils/special-characters.ts';

const MAIL = 'simon.jadwiczek@scenesofnature.earth';
const KEY_SERVER_LINK = 'https://keys.openpgp.org/search?q=simon.jadwiczek%40scenesofnature.earth';
const SHOW_COPY_DONE_ICON_DURATION = 2500;

const Contact = () => {
  const [showCopyDoneIcon, setShowCopyDoneIcon] = useState(false);

  const onCopyClick = () => {
    window.navigator.clipboard
      .writeText(MAIL)
      .then(() => {
        setShowCopyDoneIcon(true);
        setTimeout(() => setShowCopyDoneIcon(false), SHOW_COPY_DONE_ICON_DURATION);
      })
      .catch(() => console.error('Couldn\'t copy to clipboard'));
  };

  return (
    <>
      <DialogContentText variant='code1' sx={{userSelect: 'text'}}>Simon Jadwiczek</DialogContentText>
      <DialogContentText variant='code1'>
        <Link href={`mailto:${MAIL}`}>simon.jadwiczek@{ZERO_WIDTH_SPACE}scenesofnature.earth</Link>
      </DialogContentText>
      <DialogContentText mt={1.5}>
        <Button variant='text' size='small' color='inherit'
                startIcon={showCopyDoneIcon ? <DoneAllOutlinedIcon color='secondary'/> : <ContentCopyOutlinedIcon/>}
                onClick={onCopyClick}>
          <Typography variant='code2'>{showCopyDoneIcon ? 'copied' : 'copy'}</Typography>
        </Button>
      </DialogContentText>
      <DialogContentText variant='subtitle2' color='inherit' mt={4} mb={1}>Encryption</DialogContentText>
      <DialogContentText variant='code1'>For encrypted mails, you can use this key:</DialogContentText>
      <DialogContentText variant='code1'>
        <Link href={pgpKey} download>PGP key</Link>
        {' ('}
        <Typography variant='code1' sx={{userSelect: 'text'}}>0xD1793A7AF417E3C6</Typography>
        {') '}
        <Link href={KEY_SERVER_LINK} target='_blank' rel='noreferrer'>(key server)</Link>
      </DialogContentText>
    </>
  );
};

export default Contact;
