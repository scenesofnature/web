import {Button} from '@mui/material';

type FooterButtonProps = {
  readonly text: string;
  readonly onClick: () => void;
};

const FooterButton = (props: FooterButtonProps) => (
  <Button color='inherit' size='small' sx={{minWidth: 'unset', '&:hover': {backgroundColor: 'background.paper'}}} onClick={props.onClick}>
    {props.text}
  </Button>
);

export default FooterButton;
