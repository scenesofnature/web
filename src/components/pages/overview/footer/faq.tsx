import {DialogContentText, Link} from '@mui/material';

const CC_BY_NC = 'https://creativecommons.org/licenses/by-nc/4.0/';
const REPOSITORY = 'gitlab.com/scenesofnature/web';
const PHOTO_REPOSITORY_URL = 'https://gitlab.com/scenesofnature/photos';

const Faq = () => (
  <>
    <DialogContentText variant='subtitle2' color='inherit' mb={1}>Can I use the pictures?</DialogContentText>
    <DialogContentText variant='code1'>
      {'All photos on the website are licenced under '}
      <Link href={CC_BY_NC} target='_blank' rel='noreferrer'>CC BY-NC 4.0</Link>
      {' (so it depends on how you want to use them).'}
    </DialogContentText>
    <DialogContentText variant='code1'>
      {'Full resolution images are available '}
      <Link href={PHOTO_REPOSITORY_URL} target='_blank' rel='noreferrer'>here</Link>
      .
    </DialogContentText>
    <DialogContentText variant='subtitle2' color='inherit' mt={4} mb={1}>How did you build the website?</DialogContentText>
    <DialogContentText variant='code1'>See for yourself, it's open source:</DialogContentText>
    <DialogContentText variant='code1'>
      <Link href={`https://${REPOSITORY}`} target='_blank' rel='noreferrer'>{REPOSITORY}</Link>
    </DialogContentText>
    <DialogContentText variant='subtitle2' color='inherit' mt={4} mb={1}>What does the favicon/logo mean?</DialogContentText>
    <DialogContentText variant='code1'>It shows two ravens.</DialogContentText>
    <DialogContentText variant='code1'>Why? Because ravens are hella rad!</DialogContentText>
  </>
);

export default Faq;
