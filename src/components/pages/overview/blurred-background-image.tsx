import {Box} from '@mui/material';
import {tss} from 'tss-react/mui';
import {Image} from '../../../utils/image.ts';
import ResponsivePicture from '../../shared/responsive-picture.tsx';

type BlurredBackgroundImageProps = {
  readonly image: Image;
};

const useStyles = tss.create({
  blurredBackgroundPicture: {
    height: '100%',
    width: '100%',
    objectFit: 'cover',
    filter: 'blur(24px)',
    transform: 'scale(1.1)'
  },
  zoomingPicture: {
    height: '100%',
    width: '100%',
    objectFit: 'contain',
    transition: 'transform 0.3s ease',
    '&:hover': {
      transform: 'scale(1.2)'
    }
  }
});

const BlurredBackgroundImage = (props: BlurredBackgroundImageProps) => {
  const {classes} = useStyles();

  return (
    <Box height='100%' width='100%' position='relative' overflow='hidden'>
      <Box position='absolute' top={0} bottom={0} left={0} right={0}>
        <ResponsivePicture image={props.image} sizes='100vw' srcFallback={props.image.urls.webp_1024}
                           className={classes.blurredBackgroundPicture}/>
      </Box>
      <Box position='absolute' top={0} bottom={0} left={0} right={0}>
        <ResponsivePicture image={props.image} sizes='100vw' srcFallback={props.image.urls.webp_1024}
                           className={classes.zoomingPicture}/>
      </Box>
    </Box>
  );
};

export default BlurredBackgroundImage;
