import {Box, Typography} from '@mui/material';
import {useState} from 'react';
import {tss} from 'tss-react/mui';
import {Image} from '../../../utils/image.ts';
import ResponsivePicture from '../../shared/responsive-picture.tsx';

type ChapterTileProps = {
  readonly title: string;
  readonly image: Image;
  readonly moveImageX?: number;
  readonly moveImageY?: number;
  readonly onClick: () => void;
};

const useStyles = tss
  .withParams<{ moveImageX?: number, moveImageY?: number }>()
  .create(({moveImageX, moveImageY}) => ({
    picture: {
      height: '100%',
      width: '100%',
      objectFit: 'cover',
      transform: `scale(1.6) translate(${moveImageX ? `${moveImageX}%` : 0}, ${moveImageY ? `${moveImageY}%` : 0})`,
      transition: 'filter 0.3s ease, transform 0.3s ease'
    }
  }));

const ChapterTile = (props: ChapterTileProps) => {
  const {classes, cx} = useStyles({moveImageX: props.moveImageX, moveImageY: props.moveImageY});
  const [isHovered, setIsHovered] = useState(false);

  return (
    <Box position='relative'
         overflow='hidden'
         sx={{
           '&:hover': {
             '.chapter-tile-img': {
               filter: 'blur(4px)',
               transform: 'scale(1.1) translate(0)'
             },
             '.chapter-tile-title': {
               opacity: 1
             }
           }
         }}
         onMouseEnter={() => setIsHovered(true)} onMouseLeave={() => setIsHovered(false)}>
      <Box position='absolute' top={0} bottom={0} left={0} right={0}>
        <ResponsivePicture image={props.image} sizes='100vw' srcFallback={props.image.urls.webp_1024}
                           className={cx('chapter-tile-img', classes.picture)}/>
      </Box>
      <Box position='absolute' top={0} bottom={0} left={0} right={0}
           display='flex' alignItems='center' justifyContent='center'
           px={4}>
        <Typography variant='overline' color='common.white'
                    className='chapter-tile-title'
                    sx={{
                      opacity: 0,
                      transition: 'opacity 0.3s ease'
                    }}>
          {props.title}
        </Typography>
      </Box>
      <Box position='absolute' top={0} bottom={0} left={0} right={0}
           display={isHovered ? 'block' : 'none'}
           sx={{cursor: 'pointer'}}
           onClick={props.onClick}/>
    </Box>
  );
};

export default ChapterTile;
