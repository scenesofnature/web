import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import {Box, Divider, IconButton, Typography} from '@mui/material';
import {tss} from 'tss-react/mui';
import {aboutImage} from '../../../assets/photos/general.ts';
import {footerDialogueQueries, queries} from '../../../bootstrapping/navigation/queries.ts';
import {routes} from '../../../bootstrapping/navigation/routes.ts';
import {useGlobalStyles} from '../../../bootstrapping/theming/global-styles.ts';
import {useEscapeKeyListener} from '../../../utils/hooks/use-escape-key-listener.ts';
import {useIsLandscapeOrientation} from '../../../utils/hooks/use-is-landscape-orientation.ts';
import {useMouseWheelListener} from '../../../utils/hooks/use-mouse-wheel-listener.ts';
import {useSwipeListener} from '../../../utils/hooks/use-swipe-listener.ts';
import {PageProps} from '../../../utils/props.ts';
import ResponsivePicture from '../../shared/responsive-picture.tsx';
import AboutText from './about-text.tsx';

const useStyles = tss.create({
  aboutImage: {
    height: '100%',
    width: '100%',
    objectFit: 'contain'
  }
});

const AboutPage = (props: PageProps) => {
  const {classes: globalClasses} = useGlobalStyles();
  const {classes} = useStyles();
  const isLandscape = useIsLandscapeOrientation();

  useMouseWheelListener({rightListener: () => props.handleNavigation(routes.overview, 'mouse-wheel')}, props.isCurrentPage);
  useSwipeListener({leftListener: () => props.handleNavigation(routes.overview, 'swipe')}, props.isCurrentPage);
  useEscapeKeyListener(() => props.handleNavigation(routes.overview, 'button'), props.isCurrentPage);

  const navigateToContact = () => props.handleNavigation(
    routes.overview,
    'button',
    new URLSearchParams([[queries.footerDialogue, footerDialogueQueries.contact]]));

  return (
    <Box height='100%' width='100%' minWidth='100%' display='flex' flexDirection='column' className={globalClasses.themeSwitched}>
      <Box py='3vh' display='flex' justifyContent='space-between' alignItems='center'>
        <Box flex={1}/>
        <Typography variant='h2'>Hi, I'm Simon!</Typography>
        <Box flex={1} display='flex' justifyContent='flex-end'>
          <IconButton sx={{mr: '3vw'}} aria-label='back to overview' onClick={() => props.handleNavigation(routes.overview, 'button')}>
            <CloseOutlinedIcon/>
          </IconButton>
        </Box>
      </Box>
      <Box flex={1} width='100%' minHeight={0}
           display='flex' flexDirection={isLandscape ? 'row-reverse' : 'column'}
           py='3vh'
           overflow={isLandscape ? undefined : 'auto'}>
        <Box height={isLandscape ? '100%' : undefined} width={isLandscape ? '33vw' : '100%'}
             mb={isLandscape ? undefined : '4vh'} px='max(20px, 3vw)'
             overflow={isLandscape ? 'auto' : undefined}>
          <AboutText onContactClick={navigateToContact}/>
        </Box>
        {isLandscape &&
          <Divider orientation='vertical'/>
        }
        <Box flex={1} display='flex' flexDirection='column' justifyContent='center' px={isLandscape ? 'max(20px, 3vw)' : undefined}>
          {isLandscape ?
            <Box flex={1} position='relative'>
              <Box position='absolute' top={0} bottom={0} left={0} right={0}>
                <ResponsivePicture image={aboutImage} sizes='100vw' srcFallback={aboutImage.urls.webp_1024}
                                   className={classes.aboutImage}/>
              </Box>
            </Box>
            :
            <ResponsivePicture image={aboutImage} sizes='100vw' srcFallback={aboutImage.urls.webp_1024}/>
          }
          <Box id='test' display='flex' flexDirection='column' justifyContent='center' pt='1vh'>
            <Typography variant='caption'>{aboutImage.description}</Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default AboutPage;
