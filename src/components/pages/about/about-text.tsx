import {Link, Typography} from '@mui/material';
import {ChildrenProps} from '../../../utils/props.ts';

type AboutTextProps = {
  readonly onContactClick: () => void;
};

const BioHeading = (props: ChildrenProps) => (
  <Typography variant='h4' mt='2em' mb='1em' sx={{'&:first-of-type': {mt: 0}}}>
    {props.children}
  </Typography>
);

const BioParagraph = (props: ChildrenProps) => (
  <Typography variant='body1' color='text.secondary' mt='1em'>
    {props.children}
  </Typography>
);

const AboutText = (props: AboutTextProps) => (
  <>
    <BioHeading>TL;DR</BioHeading>
    <BioParagraph>
      Although I am a professional web developer since 2019, I want to contribute to something of greater value than just another industrial
      application for process optimisation.
    </BioParagraph>
    <BioParagraph>
      During a career break in 2022 I discovered my passion for photography whilst travelling through seven countries across the globe.
      Since then I determined I wanted to devote my energy and skills to preserving our planet and its wildlife in whatever way I can.
    </BioParagraph>
    <BioParagraph>
      I don't know yet how much space this vocation will take up in my life, but I'm excited to see where this journey will take me.
    </BioParagraph>
    <BioHeading>Software development</BioHeading>
    <BioParagraph>
      My journey didn't start with photography, but as a full-stack web developer. In 2019, I started to work on industrial web
      applications. By 2021, I had finished an apprenticeship as a Computer Science Expert for Software Development. Currently I'm
      finishing my Bachelor of Science in Computer Science. Up to now, I have worked on six customer projects, mainly in the mechanical
      engineering industry.
    </BioParagraph>
    <BioParagraph>
      In this profession, I not only learned a lot about technologies like .NET, React and Kubernetes, but more importantly, I learned about
      taking responsibility, handling complexity and self-leadership.
    </BioParagraph>
    <BioHeading>An eventful year</BioHeading>
    <BioParagraph>
      While I love my work with web applications in a self organised team of professionals, I am also deeply concerned about global
      developments like the transgression of our planetary boundaries (e.g. the sixth extinction event), the rise of conflicts and violence
      and society's indifference to it. Learning about these challenges, I started pondering whether I couldn't have a more meaningful
      impact on our planet than with building applications for industrial process optimisation.
    </BioParagraph>
    <BioParagraph>
      Due to a mixture of these thoughts, a good opportunity and personal reasons, I applied for a year of unpaid leave with my employer and
      for two break semesters at my university, which allowed me to take a gap year from August 2022 to July 2023. During this time, I
      embarked on a backpacking trip which led me through seven countries around the globe, funded by volunteering for board and lodging
      along the way. I wanted to use this time to acquire different skills, to learn more about foreign cultures and ultimately also about
      myself.
    </BioParagraph>
    <BioParagraph>Looking back now, I couldn't have spent the year any better.</BioParagraph>
    <BioHeading>Photography</BioHeading>
    <BioParagraph>
      From the very start, it was important for me to document this trip, so a camera was one of the first things on my packing list. After
      some considerations, I bought myself a second-hand Canon PowerShot G7 X Mark II.
    </BioParagraph>
    <BioParagraph>This was to be the beginning of my passion for photography.</BioParagraph>
    <BioParagraph>
      Documenting the experiences of my travels was one of the most fun parts of the journey itself, despite often reaching the limits of
      the camera. So when I returned back home, I quickly decided I wanted to take up photography on a more professional level. Therefore, I
      got myself a mirrorless full frame camera and I started to learn image processing. I also acquired a drone license for aerial footage.
    </BioParagraph>
    <BioParagraph>
      Today I try to take every opportunity to go out with my camera. I mostly focus on the local wildlife, but I also enjoy documenting
      outdoor activities, and I'm already looking forward to the next travels.
    </BioParagraph>
    <BioHeading>Next steps of my journey</BioHeading>
    <BioParagraph>
      I want to find ways in which I can contribute to something meaningful. Hence, I'm planning to support an animal welfare
      organisation or a similar NGO for several months after my graduation in spring 2025 (full-time).
    </BioParagraph>
    <BioParagraph>
      {'If you work for an organisation where I can do so and you have use for my skill set, I would be grateful if you '}
      <Link component='button' variant='body1' onClick={props.onContactClick}>contacted</Link>
      {' me.'}
    </BioParagraph>
    <BioHeading>Some more facts</BioHeading>
    <BioParagraph>I love music</BioParagraph>
    <BioParagraph>I like to explore the unknown{'\n'}(learning new skills, travelling new territories, ...)</BioParagraph>
    <BioParagraph>I do a lot of mountain activities</BioParagraph>
    <BioParagraph>I'm certified to conduct lead climbing training</BioParagraph>
    <BioParagraph>I'm interested in first aid skills and emergency medicine</BioParagraph>
    <BioParagraph>I live vegan</BioParagraph>
  </>
);

export default AboutText;
