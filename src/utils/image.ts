export const cameras = {
  powerShotG7Xii: 'Canon PowerShot G7 X Mk II',
  r6ii: 'Canon EOS R6 Mk II'
} as const;

type Camera = typeof cameras[keyof typeof cameras];

export const lenses = {
  rf70_200: 'Canon RF 70-200mm F2.8 L IS USM',
  ef300: 'Canon EF 300 F2.8 L IS USM',
  ef500: 'Canon EF 500 F4 L IS USM'
} as const;

type Lens = typeof lenses[keyof typeof lenses];

type Aperture = `f/${number}`;
type ShutterSpeed = `${number}/${number}` | `${number}`;

type ImageUrls = {
  readonly avif_480: string;
  readonly avif_640: string;
  readonly avif_720: string;
  readonly avif_768: string;
  readonly avif_1024: string;
  readonly avif_1366: string;
  readonly avif_1440: string;
  readonly avif_1920: string;
  readonly avif_2560: string;
  readonly avif_3840: string;
  readonly webp_480: string;
  readonly webp_640: string;
  readonly webp_720: string;
  readonly webp_768: string;
  readonly webp_1024: string;
  readonly webp_1366: string;
  readonly webp_1440: string;
  readonly webp_1920: string;
  readonly webp_2560: string;
  readonly webp_3840: string;
}

export type Image = {
  readonly urls: ImageUrls;
  readonly description: string;
  readonly camera: Camera;
  readonly lens?: Lens;
  readonly aperture: Aperture;
  readonly shutterSpeed: ShutterSpeed;
  readonly iso: number;
};

export const getAvifSrcSet = (urls: ImageUrls) => `
  ${urls.avif_480} 480w,
  ${urls.avif_640} 640w,
  ${urls.avif_720} 720w,
  ${urls.avif_768} 768w,
  ${urls.avif_1024} 1024w,
  ${urls.avif_1366} 1366w,
  ${urls.avif_1440} 1440w,
  ${urls.avif_1920} 1920w,
  ${urls.avif_2560} 2560w,
  ${urls.avif_3840} 3840w`;

export const getWebpSrcSet = (urls: ImageUrls) => `
  ${urls.webp_480} 480w,
  ${urls.webp_640} 640w,
  ${urls.webp_720} 720w,
  ${urls.webp_768} 768w,
  ${urls.webp_1024} 1024w,
  ${urls.webp_1366} 1366w,
  ${urls.webp_1440} 1440w,
  ${urls.webp_1920} 1920w,
  ${urls.webp_2560} 2560w,
  ${urls.webp_3840} 3840w`;
