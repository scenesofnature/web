export type DirectionListeners = {
  readonly upListener?: () => void;
  readonly downListener?: () => void;
  readonly leftListener?: () => void;
  readonly rightListener?: () => void
};
