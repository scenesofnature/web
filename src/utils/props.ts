import {ReactNode} from 'react';
import {NavigationHandler} from '../bootstrapping/navigation/slide-navigator.tsx';

export type ChildrenProps = {
  readonly children?: ReactNode;
};

export type PageProps = {
  readonly isCurrentPage: boolean;
  readonly handleNavigation: NavigationHandler;
};
