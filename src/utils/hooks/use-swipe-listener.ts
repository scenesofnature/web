import {useEffect} from 'react';
import {DirectionListeners} from '../direction-listeners.ts';

const REQUIRED_DIAGONAL_DIFFERENCE = 0.7;
const MIN_REQUIRED_SWIPE_DISTANCE = 100;

export const useSwipeListener = (swipeListeners: DirectionListeners, isActive = true) => {
  useEffect(() => {
    let touchStartX = 0;
    let touchStartY = 0;
    let lastTouchY = 0;

    const touchStartListener = (e: TouchEvent) => {
      if (!isActive || e.changedTouches.length !== 1) {
        return;
      }

      const touch = e.touches[0];
      touchStartX = touch.clientX;
      touchStartY = touch.clientY;
      lastTouchY = touch.clientY;
    };

    const touchMoveListener = (e: TouchEvent) => {
      // disable pull-to-refresh if there is a swipe down listener
      if (!isActive || e.changedTouches.length !== 1 || !swipeListeners.downListener) {
        return;
      }

      const touchY = e.touches[0].clientY;
      const touchYDelta = touchY - lastTouchY;
      lastTouchY = touchY;
      if (touchYDelta > 0) {
        e.preventDefault();
      }
    };

    const touchEndListener = (e: TouchEvent) => {
      if (!isActive || e.changedTouches.length !== 1) {
        return;
      }

      const touch = e.changedTouches[0];
      const deltaX = touch.clientX - touchStartX;
      const deltaY = touch.clientY - touchStartY;


      if ((Math.abs(deltaY) * REQUIRED_DIAGONAL_DIFFERENCE) > Math.abs(deltaX)) {
        // vertical swipe
        if (Math.abs(deltaY) > MIN_REQUIRED_SWIPE_DISTANCE) {
          if (deltaY > 0) {
            swipeListeners.downListener?.();
          } else {
            swipeListeners.upListener?.();
          }
        }
      } else if ((Math.abs(deltaX) * REQUIRED_DIAGONAL_DIFFERENCE) > Math.abs(deltaY)) {
        // horizontal swipe
        if (Math.abs(deltaX) > MIN_REQUIRED_SWIPE_DISTANCE) {
          if (deltaX > 0) {
            swipeListeners.rightListener?.();
          } else {
            swipeListeners.leftListener?.();
          }
        }
      }
    };

    document.addEventListener('touchstart', touchStartListener, {passive: true});
    document.addEventListener('touchmove', touchMoveListener, {passive: false});
    document.addEventListener('touchend', touchEndListener, {passive: true});

    return () => {
      document.removeEventListener('touchstart', touchStartListener);
      document.removeEventListener('touchmove', touchMoveListener);
      document.removeEventListener('touchend', touchEndListener);
    };
  }, [isActive, swipeListeners]);
};
