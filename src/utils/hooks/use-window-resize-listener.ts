import {useEffect, useLayoutEffect} from 'react';

export const useWindowResizeListener = (listener: () => void) => {
  useEffect(() => {
    const eventType = 'resize';
    window.addEventListener(eventType, listener);
    return () => window.removeEventListener(eventType, listener);
  }, [listener]);
};

export const useLayoutWindowResizeListener = (listener: () => void) => {
  useLayoutEffect(() => {
    const eventType = 'resize';
    window.addEventListener(eventType, listener);
    return () => window.removeEventListener(eventType, listener);
  }, [listener]);
};
