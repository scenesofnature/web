import {useEffect} from 'react';

export const useLocationListener = (listener: () => void) => {
  useEffect(() => {
    const eventType = 'popstate';
    window.addEventListener(eventType, listener);
    return () => window.removeEventListener(eventType, listener);
  }, [listener]);
};
