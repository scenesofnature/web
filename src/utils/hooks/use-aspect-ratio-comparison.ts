import {RefObject, useCallback, useLayoutEffect, useState} from 'react';
import {useLayoutWindowResizeListener} from './use-window-resize-listener.ts';

export const useAspectRatioComparison = (element: RefObject<HTMLElement>, _operator: '>', aspectRatio: number) => {
  const calculateIsGreater = useCallback(
    () => element.current
      ? element.current.offsetWidth / element.current.offsetHeight > aspectRatio
      : undefined,
    [element, aspectRatio]);

  const [isAspectRatioGreaterThan, setIsAspectRatioGreaterThan] = useState<boolean>();

  useLayoutEffect(() => setIsAspectRatioGreaterThan(calculateIsGreater()), [calculateIsGreater]);
  useLayoutWindowResizeListener(() => setIsAspectRatioGreaterThan(calculateIsGreater()));

  return isAspectRatioGreaterThan;
};
