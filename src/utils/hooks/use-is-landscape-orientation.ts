import {useState} from 'react';
import {useWindowResizeListener} from './use-window-resize-listener.ts';

// Firefox behaves quite unreliably regarding both window.screen.orientation event and orientationchange event (despite specs) as of version 126.
// Hence, resize event and window measurements are used to detect changes.
export const useIsLandscapeOrientation = () => {
  const getIsLandscape = () => window.innerHeight <= window.innerWidth;
  const [isLandscape, setIsLandscape] = useState(getIsLandscape());

  useWindowResizeListener(() => setIsLandscape(getIsLandscape()));
  return isLandscape;
};
