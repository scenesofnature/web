import {useEffect} from 'react';
import {DirectionListeners} from '../direction-listeners.ts';

const REQUIRED_DIAGONAL_DIFFERENCE = 0.8;

export const useMouseWheelListener = (wheelListeners: DirectionListeners, isActive: boolean) => {
  useEffect(() => {
    const listener = (e: WheelEvent) => {
      if (!isActive || e.ctrlKey) {
        return;
      }

      const invertAxis = e.shiftKey;
      const deltaX = invertAxis ? e.deltaY : e.deltaX;
      const deltaY = invertAxis ? e.deltaX : e.deltaY;

      if ((Math.abs(deltaY) * REQUIRED_DIAGONAL_DIFFERENCE) > Math.abs(deltaX)) {
        // vertical wheel movement
        if (deltaY > 0) {
          wheelListeners.downListener?.();
        } else {
          wheelListeners.upListener?.();
        }
      } else if ((Math.abs(deltaX) * REQUIRED_DIAGONAL_DIFFERENCE) > Math.abs(deltaY)) {
        // horizontal wheel movement
        if (deltaX > 0) {
          wheelListeners.rightListener?.();
        } else {
          wheelListeners.leftListener?.();
        }
      }
    };

    const eventType = 'wheel';
    window.addEventListener(eventType, listener);
    return () => window.removeEventListener(eventType, listener);
  }, [isActive, wheelListeners]);
};
