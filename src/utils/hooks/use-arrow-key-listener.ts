import {useEffect} from 'react';
import {DirectionListeners} from '../direction-listeners.ts';

export const useArrowKeyListener = (arrowKeyListeners: DirectionListeners, isActive = true) => {
  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if (!isActive) {
        return;
      }

      switch (e.key) {
        case 'ArrowUp':
          arrowKeyListeners.upListener?.();
          break;
        case 'ArrowDown':
          arrowKeyListeners.downListener?.();
          break;
        case 'ArrowLeft':
          arrowKeyListeners.leftListener?.();
          break;
        case 'ArrowRight':
          arrowKeyListeners.rightListener?.();
          break;
      }
    };

    const eventType = 'keydown';
    document.addEventListener(eventType, listener);
    return () => document.removeEventListener(eventType, listener);
  }, [isActive, arrowKeyListeners]);
};
