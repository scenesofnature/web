import {useEffect} from 'react';

export const useEscapeKeyListener = (escapeKeyListener: () => void, isActive = true) => {
  useEffect(() => {
    const listener = (e: KeyboardEvent) => {
      if (isActive && e.key === 'Escape') {
        escapeKeyListener();
      }
    };

    const eventType = 'keydown';
    document.addEventListener(eventType, listener);
    return () => document.removeEventListener(eventType, listener);
  }, [isActive, escapeKeyListener]);
};
