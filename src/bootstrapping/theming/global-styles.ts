import {Theme} from '@mui/material';
import {GlobalStylesProps} from '@mui/system/GlobalStyles/GlobalStyles';
import {tss} from 'tss-react/mui';

export const globalStyles: GlobalStylesProps<Theme>['styles'] = {
  html: {
    height: '100%'
  },
  body: {
    height: '100%',
    whiteSpaceCollapse: 'unset'
  },
  img: {
    userSelect: 'none',
    fontSize: 0
  },
  '#root': {
    height: '100%',
    overflow: 'hidden'
  }
};

export const useGlobalStyles = tss.create(({theme}) => ({
  /* eslint-disable tss-unused-classes/unused-classes */
  themeSwitched: {
    color: theme.palette.text.primary,
    backgroundColor: theme.palette.background.default,
    scrollbarColor: theme.palette.mode === 'light' ? '#000 #fff' : '#fff #000'
  }
  /* eslint-enable tss-unused-classes/unused-classes */
}));
