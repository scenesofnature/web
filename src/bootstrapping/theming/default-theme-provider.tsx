import {CssBaseline, GlobalStyles, ThemeProvider} from '@mui/material';
import {ChildrenProps} from '../../utils/props.ts';
import {globalStyles} from './global-styles.ts';
import {makeTheme} from './themes.ts';
import '../../assets/fonts/m-plus-code-latin/m-plus-code-latin.css';
import '../../assets/fonts/raleway/raleway.css';

const lightTheme = makeTheme('light');
const darkTheme = makeTheme('dark');
const cssBaseline = <CssBaseline/>;
const globalStylesProvider = <GlobalStyles styles={globalStyles}/>;

export const DefaultThemeProvider = (props: ChildrenProps) => (
  <ThemeProvider theme={lightTheme}>
    {cssBaseline}
    {globalStylesProvider}
    {props.children}
  </ThemeProvider>
);

export const DarkThemeProvider = (props: ChildrenProps) => (
  <ThemeProvider theme={darkTheme}>
    {props.children}
  </ThemeProvider>
);
