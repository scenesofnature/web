import {createTheme, PaletteMode, responsiveFontSizes, ThemeOptions} from '@mui/material';
import {blood, sand} from './palettes.ts';

const lightThemeOptions: ThemeOptions = {
  spacing: 4,
  palette: {
    mode: 'light',
    primary: {
      main: sand[900]
    },
    secondary: {
      main: blood[500]
    },
    text: {
      primary: '#000',
      secondary: 'rgba(0, 0, 0, 0.75)'
    },
    background: {
      default: '#fff',
      paper: sand[300]
    }
  },
  typography: {
    fontFamily: 'Raleway, sans-serif',
    h1: {
      fontSize: '4rem', // 64px
      fontWeight: 280,
      lineHeight: 1.25, // 80px
      letterSpacing: '0.1875em', // 3px
      textAlign: 'center',
      whiteSpaceCollapse: 'preserve-breaks',
      userSelect: 'none'
    },
    h2: {
      fontSize: '2rem', // 32px
      fontWeight: 200,
      lineHeight: 1.25, // 40px
      letterSpacing: '0.125em', // 2px
      textAlign: 'center',
      whiteSpaceCollapse: 'preserve-breaks',
      userSelect: 'none'
    },
    h3: {
      fontSize: '1.75rem', // 28
      fontWeight: 240,
      lineHeight: 1.2857, // 36px
      letterSpacing: '0.0625em', // 1px
      textAlign: 'center',
      whiteSpaceCollapse: 'preserve-breaks',
      userSelect: 'none'
    },
    h4: {
      fontSize: '1.5rem', // 24
      fontWeight: 280,
      lineHeight: 1.1667, // 28px
      letterSpacing: '0.0625em', // 1px
      whiteSpaceCollapse: 'preserve-breaks',
      userSelect: 'none'
    },
    subtitle1: {
      fontSize: '1.1875rem', // 19px
      fontWeight: 840,
      lineHeight: 1.2632, // 24px
      letterSpacing: 0,
      whiteSpaceCollapse: 'preserve-breaks',
      userSelect: 'none'
    },
    subtitle2: {
      fontSize: '0.9375rem', // 15px
      fontWeight: 900,
      lineHeight: 1.3333, // 20px
      letterSpacing: 0,
      whiteSpaceCollapse: 'preserve-breaks',
      userSelect: 'none'
    },
    body1: {
      fontSize: '1.1875rem', // 19px
      fontWeight: 520,
      lineHeight: 1.2632, // 24px
      letterSpacing: '0.025em', // 0.4px
      whiteSpaceCollapse: 'preserve-breaks'
    },
    body2: {
      fontSize: '1.0625rem', // 17px
      fontWeight: 520,
      lineHeight: 1.1765, // 20px
      letterSpacing: '0.025em', // 0.4px
      whiteSpaceCollapse: 'preserve-breaks'
    },
    button: {
      fontSize: '0.9375rem', // 15px
      fontWeight: 700,
      lineHeight: 1.3333, // 20px
      letterSpacing: '0.1em', // 1.6px
      textTransform: 'uppercase',
      userSelect: 'none'
    },
    caption: {
      fontSize: '0.9375rem', // 15px
      fontWeight: 220,
      lineHeight: 1.4, // 21px
      letterSpacing: '0.0375em', // 0.6px
      whiteSpaceCollapse: 'preserve-breaks',
      textAlign: 'center'
    },
    overline: {
      fontSize: '2rem', // 32px
      fontWeight: 500,
      lineHeight: 1.25, // 40px
      letterSpacing: '0.0625em', // 1px
      textAlign: 'center',
      textTransform: 'uppercase',
      whiteSpaceCollapse: 'preserve-breaks',
      userSelect: 'none'
    },
    code1: {
      fontFamily: '"M+ Code Latin", monospace',
      fontSize: '0.9375rem', // 15px
      fontWeight: 400,
      fontStretch: '125%',
      lineHeight: 1.3333, // 20px
      whiteSpaceCollapse: 'preserve',
      userSelect: 'none'
    },
    code2: {
      fontFamily: '"M+ Code Latin", monospace',
      fontSize: '0.6875rem', // 11px
      fontWeight: 360,
      fontStretch: '118%',
      lineHeight: 1.4545, // 16px
      whiteSpaceCollapse: 'preserve',
      userSelect: 'none'
    }
  },
  components: {
    MuiTypography: {
      defaultProps: {
        variantMapping: {
          code1: 'code',
          code2: 'code'
        }
      }
    },
    MuiLink: {
      styleOverrides: {
        root: ({ownerState}) => ({
          overflowWrap: 'break-word',
          userSelect: ownerState.component === 'button' ? 'inherit' : undefined,
          verticalAlign: ownerState.component === 'button' ? 'inherit' : undefined
        })
      }
    },
    MuiButton: {
      defaultProps: {
        disableElevation: true,
        disableFocusRipple: true,
        tabIndex: -1
      }
    },
    MuiIconButton: {
      defaultProps: {
        tabIndex: -1
      }
    },
    MuiDialog: {
      defaultProps: {
        PaperProps: {
          sx: {
            backgroundColor: 'background.default'
          }
        }
      }
    }
  }
};

const darkThemeOptions: ThemeOptions = {
  ...lightThemeOptions,
  palette: {
    mode: 'dark',
    primary: {
      main: sand[500]
    },
    secondary: {
      main: blood[500]
    },
    text: {
      primary: '#fff',
      secondary: 'rgba(255, 255, 255, 0.75)'
    },
    background: {
      default: '#000',
      paper: '#000'
    }
  },
  components: {
    MuiLink: {
      styleOverrides: {
        root: ({ownerState}) => ({
          overflowWrap: 'break-word',
          userSelect: ownerState.component === 'button' ? 'inherit' : undefined,
          verticalAlign: ownerState.component === 'button' ? 'inherit' : undefined
        })
      }
    }
  }
};

export const makeTheme = (mode: PaletteMode) => responsiveFontSizes(
  createTheme(mode === 'light' ? lightThemeOptions : darkThemeOptions),
  {factor: 4, disableAlign: true}
);
