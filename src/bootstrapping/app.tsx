import SlideNavigator from './navigation/slide-navigator.tsx';
import {DefaultThemeProvider} from './theming/default-theme-provider.tsx';

const App = () => (
  <DefaultThemeProvider>
    <SlideNavigator/>
  </DefaultThemeProvider>
);

export default App;
