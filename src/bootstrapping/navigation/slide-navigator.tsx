import {Box} from '@mui/material';
import {useEffect, useRef, useState} from 'react';
import {tss} from 'tss-react/mui';
import AboutPage from '../../components/pages/about/about.page.tsx';
import GalleryPage from '../../components/pages/gallery/gallery.page.tsx';
import LandingPage from '../../components/pages/landing.page.tsx';
import OverviewPage from '../../components/pages/overview/overview.page.tsx';
import {useLocationListener} from '../../utils/hooks/use-location-listener.ts';
import {DarkThemeProvider} from '../theming/default-theme-provider.tsx';
import {getCurrentQuery} from './queries.ts';
import {getCurrentPageAndAssureExists, Page, routes} from './routes.ts';
import {createTitle} from './title-creator.ts';

type NavigationTrigger = 'button' | 'mouse-wheel' | 'swipe';
export type NavigationHandler = (to: Page, trigger: NavigationTrigger, query?: URLSearchParams, replaceUrl?: boolean) => void;

const PAGE_COLUMNS = 3;
const PAGE_ROWS = 2;

const MOUSE_WHEEL_SCROLL_TIMEOUT_DURATION = 500;
const SWIPE_SCROLL_TIMEOUT_DURATION = 800;

const useStyles = tss.create(({theme}) => ({
  gridItem: {
    minHeight: '100%',
    maxHeight: '100%',
    minWidth: '100%',
    maxWidth: '100%'
  },
  emptyGridItem: {
    backgroundColor: theme.palette.common.black
  },
  showLandingPage: {
    transform: `translateX(calc(100% / ${PAGE_COLUMNS} * -1)) translateY(0)`
  },
  showOverviewPage: {
    transform: `translateX(calc(100% / ${PAGE_COLUMNS} * -1)) translateY(calc(100% / ${PAGE_ROWS} * -1))`
  },
  showAboutPage: {
    transform: `translateX(0) translateY(calc(100% / ${PAGE_ROWS} * -1))`
  },
  showGalleryPage: {
    transform: `translateX(calc(100% / ${PAGE_COLUMNS} * -2)) translateY(calc(100% / ${PAGE_ROWS} * -1))`
  }
}));

const SlideNavigator = () => {
  const {classes, cx} = useStyles();
  const isNavigationTimeout = useRef(false);
  const [currentPage, setCurrentPage] = useState<Page>(getCurrentPageAndAssureExists());

  const handleNavigation: NavigationHandler = (to, trigger, query = new URLSearchParams(), replaceUrl) => {
    if (trigger !== 'button') {
      if (isNavigationTimeout.current) {
        return;
      }

      const timeout = trigger === 'mouse-wheel'
        ? MOUSE_WHEEL_SCROLL_TIMEOUT_DURATION
        : SWIPE_SCROLL_TIMEOUT_DURATION;

      isNavigationTimeout.current = true;
      setTimeout(() => {
        isNavigationTimeout.current = false;
      }, timeout);
    }

    setCurrentPage(to);

    const url = new URL(window.location.href);
    url.pathname = to;
    url.search = query.toString();

    if (replaceUrl) {
      window.history.replaceState({}, '', url);
    } else {
      window.history.pushState({}, '', url);
    }

    document.title = createTitle(to, query);
  };

  // handle URL changes from browser back/forth navigation
  useLocationListener(() => {
    const page = getCurrentPageAndAssureExists();
    const query = getCurrentQuery();
    setCurrentPage(page);
    document.title = createTitle(page, query);
  });

  // set title on initial page load
  useEffect(() => {
    const page = getCurrentPageAndAssureExists();
    const query = getCurrentQuery();
    document.title = createTitle(page, query);
  }, []);

  const getTransformClass = () => {
    switch (currentPage) {
      case routes.landing:
        return classes.showLandingPage;
      case routes.overview:
        return classes.showOverviewPage;
      case routes.about:
        return classes.showAboutPage;
      case routes.gallery:
        return classes.showGalleryPage;
    }
  };

  return (
    <Box height={`calc(100% * ${PAGE_ROWS})`} width={`calc(100% * ${PAGE_COLUMNS})`}
         display='grid' gridTemplateColumns={`repeat(${PAGE_COLUMNS}, 1fr)`} gridTemplateRows={`repeat(${PAGE_ROWS}, 1fr)`}
         sx={{transition: 'transform 1s ease', willChange: 'transform'}}
         className={getTransformClass()}>
      <div className={cx(classes.gridItem, classes.emptyGridItem)}/>
      <div className={classes.gridItem}>
        <LandingPage isCurrentPage={currentPage === routes.landing} handleNavigation={handleNavigation}/>
      </div>
      <div className={cx(classes.gridItem, classes.emptyGridItem)}/>
      <div className={classes.gridItem}>
        <DarkThemeProvider>
          <AboutPage isCurrentPage={currentPage === routes.about} handleNavigation={handleNavigation}/>
        </DarkThemeProvider>
      </div>
      <div className={classes.gridItem}>
        <OverviewPage isCurrentPage={currentPage === routes.overview} handleNavigation={handleNavigation}/>
      </div>
      <div className={classes.gridItem}>
        <DarkThemeProvider>
          <GalleryPage isCurrentPage={currentPage === routes.gallery} handleNavigation={handleNavigation}/>
        </DarkThemeProvider>
      </div>
    </Box>
  );
};

export default SlideNavigator;
