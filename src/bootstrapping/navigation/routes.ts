export const routes = {
  landing: '/',
  about: '/about',
  overview: '/overview',
  gallery: '/gallery'
} as const;

export type Page = typeof routes[keyof typeof routes];

export const getCurrentPageAndAssureExists = (): Page => {
  let path = window.location.pathname;

  if (path !== routes.landing && path.endsWith('/')) {
    // ignore trailing slash
    path = path.substring(0, path.length - 1);
  }

  const possibleRoutes: string[] = Object.values(routes);
  if (possibleRoutes.includes(path)) {
    return path as Page;
  }

  window.history.replaceState({}, '', routes.landing);
  return routes.landing;
};
