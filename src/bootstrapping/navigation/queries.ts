export const queries = {
  footerDialogue: 'dialogue',
  chapter: 'chapter'
} as const;

export const footerDialogueQueries = {
  faq: 'faq',
  contact: 'contact',
  legal: 'legal'
} as const;

export const chapterQueries = {
  birds: 'birds',
  botswana: 'botswana',
  nepal: 'nepal',
  newZealand: 'new-zealand',
  australia: 'australia',
  canada: 'canada'
} as const;

export type FooterDialogue = typeof footerDialogueQueries[keyof typeof footerDialogueQueries];
export type Chapter = typeof chapterQueries[keyof typeof chapterQueries];

export const getCurrentQuery = () => new URLSearchParams(window.location.search);

export const getCurrentFooterDialogue = (): FooterDialogue | null => {
  const query = getCurrentQuery();
  const footerDialogue = query.get(queries.footerDialogue);

  const possibleFooterDialogues: string[] = Object.values(footerDialogueQueries);
  if (footerDialogue !== null && possibleFooterDialogues.includes(footerDialogue)) {
    return footerDialogue as FooterDialogue;
  }

  return null;
};

export const getCurrentChapter = (): Chapter | null => {
  const query = getCurrentQuery();
  const chapter = query.get(queries.chapter);

  const possibleChapters: string[] = Object.values(chapterQueries);
  if (chapter !== null && possibleChapters.includes(chapter)) {
    return chapter as Chapter;
  }

  return null;
};
