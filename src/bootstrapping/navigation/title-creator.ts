import {Chapter, chapterQueries, FooterDialogue, footerDialogueQueries, queries} from './queries.ts';
import {Page, routes} from './routes.ts';

const SCENES_OF_NATURE = 'Scenes of Nature';

export const createTitle = (page: Page, query: URLSearchParams) => {
  switch (page) {
    case routes.landing:
      return SCENES_OF_NATURE;
    case routes.about:
      return `${SCENES_OF_NATURE} - About`;
    case routes.overview:
      switch (query.get(queries.footerDialogue) as (FooterDialogue | null)) {
        case footerDialogueQueries.faq:
          return `${SCENES_OF_NATURE} - FAQ`;
        case footerDialogueQueries.contact:
          return `${SCENES_OF_NATURE} - Contact`;
        case footerDialogueQueries.legal:
          return `${SCENES_OF_NATURE} - Legal`;
        default:
          return `${SCENES_OF_NATURE} - Overview`;
      }
    case routes.gallery:
      switch (query.get(queries.chapter) as (Chapter | null)) {
        case chapterQueries.birds:
          return `${SCENES_OF_NATURE} - Birds`;
        case chapterQueries.botswana:
          return `${SCENES_OF_NATURE} - Botswana`;
        case chapterQueries.nepal:
          return `${SCENES_OF_NATURE} - Nepal`;
        case chapterQueries.newZealand:
          return `${SCENES_OF_NATURE} - New Zealand`;
        case chapterQueries.australia:
          return `${SCENES_OF_NATURE} - Australia`;
        case chapterQueries.canada:
          return `${SCENES_OF_NATURE} - Canada`;
        default:
          return SCENES_OF_NATURE;
      }
  }
};
