import {cameras, Image} from '../../utils/image.ts';

import avif_001_1024 from './generated/002-australia/001-1024.avif';
import webp_001_1024 from './generated/002-australia/001-1024.webp';
import avif_001_1366 from './generated/002-australia/001-1366.avif';
import webp_001_1366 from './generated/002-australia/001-1366.webp';
import avif_001_1440 from './generated/002-australia/001-1440.avif';
import webp_001_1440 from './generated/002-australia/001-1440.webp';
import avif_001_1920 from './generated/002-australia/001-1920.avif';
import webp_001_1920 from './generated/002-australia/001-1920.webp';
import avif_001_2560 from './generated/002-australia/001-2560.avif';
import webp_001_2560 from './generated/002-australia/001-2560.webp';
import avif_001_3840 from './generated/002-australia/001-3840.avif';
import webp_001_3840 from './generated/002-australia/001-3840.webp';
import avif_001_480 from './generated/002-australia/001-480.avif';
import webp_001_480 from './generated/002-australia/001-480.webp';
import avif_001_640 from './generated/002-australia/001-640.avif';
import webp_001_640 from './generated/002-australia/001-640.webp';
import avif_001_720 from './generated/002-australia/001-720.avif';
import webp_001_720 from './generated/002-australia/001-720.webp';
import avif_001_768 from './generated/002-australia/001-768.avif';
import webp_001_768 from './generated/002-australia/001-768.webp';

import avif_002_1024 from './generated/002-australia/002-1024.avif';
import webp_002_1024 from './generated/002-australia/002-1024.webp';
import avif_002_1366 from './generated/002-australia/002-1366.avif';
import webp_002_1366 from './generated/002-australia/002-1366.webp';
import avif_002_1440 from './generated/002-australia/002-1440.avif';
import webp_002_1440 from './generated/002-australia/002-1440.webp';
import avif_002_1920 from './generated/002-australia/002-1920.avif';
import webp_002_1920 from './generated/002-australia/002-1920.webp';
import avif_002_2560 from './generated/002-australia/002-2560.avif';
import webp_002_2560 from './generated/002-australia/002-2560.webp';
import avif_002_3840 from './generated/002-australia/002-3840.avif';
import webp_002_3840 from './generated/002-australia/002-3840.webp';
import avif_002_480 from './generated/002-australia/002-480.avif';
import webp_002_480 from './generated/002-australia/002-480.webp';
import avif_002_640 from './generated/002-australia/002-640.avif';
import webp_002_640 from './generated/002-australia/002-640.webp';
import avif_002_720 from './generated/002-australia/002-720.avif';
import webp_002_720 from './generated/002-australia/002-720.webp';
import avif_002_768 from './generated/002-australia/002-768.avif';
import webp_002_768 from './generated/002-australia/002-768.webp';

import avif_003_1024 from './generated/002-australia/003-1024.avif';
import webp_003_1024 from './generated/002-australia/003-1024.webp';
import avif_003_1366 from './generated/002-australia/003-1366.avif';
import webp_003_1366 from './generated/002-australia/003-1366.webp';
import avif_003_1440 from './generated/002-australia/003-1440.avif';
import webp_003_1440 from './generated/002-australia/003-1440.webp';
import avif_003_1920 from './generated/002-australia/003-1920.avif';
import webp_003_1920 from './generated/002-australia/003-1920.webp';
import avif_003_2560 from './generated/002-australia/003-2560.avif';
import webp_003_2560 from './generated/002-australia/003-2560.webp';
import avif_003_3840 from './generated/002-australia/003-3840.avif';
import webp_003_3840 from './generated/002-australia/003-3840.webp';
import avif_003_480 from './generated/002-australia/003-480.avif';
import webp_003_480 from './generated/002-australia/003-480.webp';
import avif_003_640 from './generated/002-australia/003-640.avif';
import webp_003_640 from './generated/002-australia/003-640.webp';
import avif_003_720 from './generated/002-australia/003-720.avif';
import webp_003_720 from './generated/002-australia/003-720.webp';
import avif_003_768 from './generated/002-australia/003-768.avif';
import webp_003_768 from './generated/002-australia/003-768.webp';

import avif_004_1024 from './generated/002-australia/004-1024.avif';
import webp_004_1024 from './generated/002-australia/004-1024.webp';
import avif_004_1366 from './generated/002-australia/004-1366.avif';
import webp_004_1366 from './generated/002-australia/004-1366.webp';
import avif_004_1440 from './generated/002-australia/004-1440.avif';
import webp_004_1440 from './generated/002-australia/004-1440.webp';
import avif_004_1920 from './generated/002-australia/004-1920.avif';
import webp_004_1920 from './generated/002-australia/004-1920.webp';
import avif_004_2560 from './generated/002-australia/004-2560.avif';
import webp_004_2560 from './generated/002-australia/004-2560.webp';
import avif_004_3840 from './generated/002-australia/004-3840.avif';
import webp_004_3840 from './generated/002-australia/004-3840.webp';
import avif_004_480 from './generated/002-australia/004-480.avif';
import webp_004_480 from './generated/002-australia/004-480.webp';
import avif_004_640 from './generated/002-australia/004-640.avif';
import webp_004_640 from './generated/002-australia/004-640.webp';
import avif_004_720 from './generated/002-australia/004-720.avif';
import webp_004_720 from './generated/002-australia/004-720.webp';
import avif_004_768 from './generated/002-australia/004-768.avif';
import webp_004_768 from './generated/002-australia/004-768.webp';

import avif_005_1024 from './generated/002-australia/005-1024.avif';
import webp_005_1024 from './generated/002-australia/005-1024.webp';
import avif_005_1366 from './generated/002-australia/005-1366.avif';
import webp_005_1366 from './generated/002-australia/005-1366.webp';
import avif_005_1440 from './generated/002-australia/005-1440.avif';
import webp_005_1440 from './generated/002-australia/005-1440.webp';
import avif_005_1920 from './generated/002-australia/005-1920.avif';
import webp_005_1920 from './generated/002-australia/005-1920.webp';
import avif_005_2560 from './generated/002-australia/005-2560.avif';
import webp_005_2560 from './generated/002-australia/005-2560.webp';
import avif_005_3840 from './generated/002-australia/005-3840.avif';
import webp_005_3840 from './generated/002-australia/005-3840.webp';
import avif_005_480 from './generated/002-australia/005-480.avif';
import webp_005_480 from './generated/002-australia/005-480.webp';
import avif_005_640 from './generated/002-australia/005-640.avif';
import webp_005_640 from './generated/002-australia/005-640.webp';
import avif_005_720 from './generated/002-australia/005-720.avif';
import webp_005_720 from './generated/002-australia/005-720.webp';
import avif_005_768 from './generated/002-australia/005-768.avif';
import webp_005_768 from './generated/002-australia/005-768.webp';

import avif_006_1024 from './generated/002-australia/006-1024.avif';
import webp_006_1024 from './generated/002-australia/006-1024.webp';
import avif_006_1366 from './generated/002-australia/006-1366.avif';
import webp_006_1366 from './generated/002-australia/006-1366.webp';
import avif_006_1440 from './generated/002-australia/006-1440.avif';
import webp_006_1440 from './generated/002-australia/006-1440.webp';
import avif_006_1920 from './generated/002-australia/006-1920.avif';
import webp_006_1920 from './generated/002-australia/006-1920.webp';
import avif_006_2560 from './generated/002-australia/006-2560.avif';
import webp_006_2560 from './generated/002-australia/006-2560.webp';
import avif_006_3840 from './generated/002-australia/006-3840.avif';
import webp_006_3840 from './generated/002-australia/006-3840.webp';
import avif_006_480 from './generated/002-australia/006-480.avif';
import webp_006_480 from './generated/002-australia/006-480.webp';
import avif_006_640 from './generated/002-australia/006-640.avif';
import webp_006_640 from './generated/002-australia/006-640.webp';
import avif_006_720 from './generated/002-australia/006-720.avif';
import webp_006_720 from './generated/002-australia/006-720.webp';
import avif_006_768 from './generated/002-australia/006-768.avif';
import webp_006_768 from './generated/002-australia/006-768.webp';

import avif_007_1024 from './generated/002-australia/007-1024.avif';
import webp_007_1024 from './generated/002-australia/007-1024.webp';
import avif_007_1366 from './generated/002-australia/007-1366.avif';
import webp_007_1366 from './generated/002-australia/007-1366.webp';
import avif_007_1440 from './generated/002-australia/007-1440.avif';
import webp_007_1440 from './generated/002-australia/007-1440.webp';
import avif_007_1920 from './generated/002-australia/007-1920.avif';
import webp_007_1920 from './generated/002-australia/007-1920.webp';
import avif_007_2560 from './generated/002-australia/007-2560.avif';
import webp_007_2560 from './generated/002-australia/007-2560.webp';
import avif_007_3840 from './generated/002-australia/007-3840.avif';
import webp_007_3840 from './generated/002-australia/007-3840.webp';
import avif_007_480 from './generated/002-australia/007-480.avif';
import webp_007_480 from './generated/002-australia/007-480.webp';
import avif_007_640 from './generated/002-australia/007-640.avif';
import webp_007_640 from './generated/002-australia/007-640.webp';
import avif_007_720 from './generated/002-australia/007-720.avif';
import webp_007_720 from './generated/002-australia/007-720.webp';
import avif_007_768 from './generated/002-australia/007-768.avif';
import webp_007_768 from './generated/002-australia/007-768.webp';

import avif_008_1024 from './generated/002-australia/008-1024.avif';
import webp_008_1024 from './generated/002-australia/008-1024.webp';
import avif_008_1366 from './generated/002-australia/008-1366.avif';
import webp_008_1366 from './generated/002-australia/008-1366.webp';
import avif_008_1440 from './generated/002-australia/008-1440.avif';
import webp_008_1440 from './generated/002-australia/008-1440.webp';
import avif_008_1920 from './generated/002-australia/008-1920.avif';
import webp_008_1920 from './generated/002-australia/008-1920.webp';
import avif_008_2560 from './generated/002-australia/008-2560.avif';
import webp_008_2560 from './generated/002-australia/008-2560.webp';
import avif_008_3840 from './generated/002-australia/008-3840.avif';
import webp_008_3840 from './generated/002-australia/008-3840.webp';
import avif_008_480 from './generated/002-australia/008-480.avif';
import webp_008_480 from './generated/002-australia/008-480.webp';
import avif_008_640 from './generated/002-australia/008-640.avif';
import webp_008_640 from './generated/002-australia/008-640.webp';
import avif_008_720 from './generated/002-australia/008-720.avif';
import webp_008_720 from './generated/002-australia/008-720.webp';
import avif_008_768 from './generated/002-australia/008-768.avif';
import webp_008_768 from './generated/002-australia/008-768.webp';

import avif_009_1024 from './generated/002-australia/009-1024.avif';
import webp_009_1024 from './generated/002-australia/009-1024.webp';
import avif_009_1366 from './generated/002-australia/009-1366.avif';
import webp_009_1366 from './generated/002-australia/009-1366.webp';
import avif_009_1440 from './generated/002-australia/009-1440.avif';
import webp_009_1440 from './generated/002-australia/009-1440.webp';
import avif_009_1920 from './generated/002-australia/009-1920.avif';
import webp_009_1920 from './generated/002-australia/009-1920.webp';
import avif_009_2560 from './generated/002-australia/009-2560.avif';
import webp_009_2560 from './generated/002-australia/009-2560.webp';
import avif_009_3840 from './generated/002-australia/009-3840.avif';
import webp_009_3840 from './generated/002-australia/009-3840.webp';
import avif_009_480 from './generated/002-australia/009-480.avif';
import webp_009_480 from './generated/002-australia/009-480.webp';
import avif_009_640 from './generated/002-australia/009-640.avif';
import webp_009_640 from './generated/002-australia/009-640.webp';
import avif_009_720 from './generated/002-australia/009-720.avif';
import webp_009_720 from './generated/002-australia/009-720.webp';
import avif_009_768 from './generated/002-australia/009-768.avif';
import webp_009_768 from './generated/002-australia/009-768.webp';

import avif_010_1024 from './generated/002-australia/010-1024.avif';
import webp_010_1024 from './generated/002-australia/010-1024.webp';
import avif_010_1366 from './generated/002-australia/010-1366.avif';
import webp_010_1366 from './generated/002-australia/010-1366.webp';
import avif_010_1440 from './generated/002-australia/010-1440.avif';
import webp_010_1440 from './generated/002-australia/010-1440.webp';
import avif_010_1920 from './generated/002-australia/010-1920.avif';
import webp_010_1920 from './generated/002-australia/010-1920.webp';
import avif_010_2560 from './generated/002-australia/010-2560.avif';
import webp_010_2560 from './generated/002-australia/010-2560.webp';
import avif_010_3840 from './generated/002-australia/010-3840.avif';
import webp_010_3840 from './generated/002-australia/010-3840.webp';
import avif_010_480 from './generated/002-australia/010-480.avif';
import webp_010_480 from './generated/002-australia/010-480.webp';
import avif_010_640 from './generated/002-australia/010-640.avif';
import webp_010_640 from './generated/002-australia/010-640.webp';
import avif_010_720 from './generated/002-australia/010-720.avif';
import webp_010_720 from './generated/002-australia/010-720.webp';
import avif_010_768 from './generated/002-australia/010-768.avif';
import webp_010_768 from './generated/002-australia/010-768.webp';

import avif_011_1024 from './generated/002-australia/011-1024.avif';
import webp_011_1024 from './generated/002-australia/011-1024.webp';
import avif_011_1366 from './generated/002-australia/011-1366.avif';
import webp_011_1366 from './generated/002-australia/011-1366.webp';
import avif_011_1440 from './generated/002-australia/011-1440.avif';
import webp_011_1440 from './generated/002-australia/011-1440.webp';
import avif_011_1920 from './generated/002-australia/011-1920.avif';
import webp_011_1920 from './generated/002-australia/011-1920.webp';
import avif_011_2560 from './generated/002-australia/011-2560.avif';
import webp_011_2560 from './generated/002-australia/011-2560.webp';
import avif_011_3840 from './generated/002-australia/011-3840.avif';
import webp_011_3840 from './generated/002-australia/011-3840.webp';
import avif_011_480 from './generated/002-australia/011-480.avif';
import webp_011_480 from './generated/002-australia/011-480.webp';
import avif_011_640 from './generated/002-australia/011-640.avif';
import webp_011_640 from './generated/002-australia/011-640.webp';
import avif_011_720 from './generated/002-australia/011-720.avif';
import webp_011_720 from './generated/002-australia/011-720.webp';
import avif_011_768 from './generated/002-australia/011-768.avif';
import webp_011_768 from './generated/002-australia/011-768.webp';

import avif_012_1024 from './generated/002-australia/012-1024.avif';
import webp_012_1024 from './generated/002-australia/012-1024.webp';
import avif_012_1366 from './generated/002-australia/012-1366.avif';
import webp_012_1366 from './generated/002-australia/012-1366.webp';
import avif_012_1440 from './generated/002-australia/012-1440.avif';
import webp_012_1440 from './generated/002-australia/012-1440.webp';
import avif_012_1920 from './generated/002-australia/012-1920.avif';
import webp_012_1920 from './generated/002-australia/012-1920.webp';
import avif_012_2560 from './generated/002-australia/012-2560.avif';
import webp_012_2560 from './generated/002-australia/012-2560.webp';
import avif_012_3840 from './generated/002-australia/012-3840.avif';
import webp_012_3840 from './generated/002-australia/012-3840.webp';
import avif_012_480 from './generated/002-australia/012-480.avif';
import webp_012_480 from './generated/002-australia/012-480.webp';
import avif_012_640 from './generated/002-australia/012-640.avif';
import webp_012_640 from './generated/002-australia/012-640.webp';
import avif_012_720 from './generated/002-australia/012-720.avif';
import webp_012_720 from './generated/002-australia/012-720.webp';
import avif_012_768 from './generated/002-australia/012-768.avif';
import webp_012_768 from './generated/002-australia/012-768.webp';

import avif_013_1024 from './generated/002-australia/013-1024.avif';
import webp_013_1024 from './generated/002-australia/013-1024.webp';
import avif_013_1366 from './generated/002-australia/013-1366.avif';
import webp_013_1366 from './generated/002-australia/013-1366.webp';
import avif_013_1440 from './generated/002-australia/013-1440.avif';
import webp_013_1440 from './generated/002-australia/013-1440.webp';
import avif_013_1920 from './generated/002-australia/013-1920.avif';
import webp_013_1920 from './generated/002-australia/013-1920.webp';
import avif_013_2560 from './generated/002-australia/013-2560.avif';
import webp_013_2560 from './generated/002-australia/013-2560.webp';
import avif_013_3840 from './generated/002-australia/013-3840.avif';
import webp_013_3840 from './generated/002-australia/013-3840.webp';
import avif_013_480 from './generated/002-australia/013-480.avif';
import webp_013_480 from './generated/002-australia/013-480.webp';
import avif_013_640 from './generated/002-australia/013-640.avif';
import webp_013_640 from './generated/002-australia/013-640.webp';
import avif_013_720 from './generated/002-australia/013-720.avif';
import webp_013_720 from './generated/002-australia/013-720.webp';
import avif_013_768 from './generated/002-australia/013-768.avif';
import webp_013_768 from './generated/002-australia/013-768.webp';

export const australiaImages: Image[] = [
  {
    urls: {
      avif_480: avif_001_480,
      avif_640: avif_001_640,
      avif_720: avif_001_720,
      avif_768: avif_001_768,
      avif_1024: avif_001_1024,
      avif_1366: avif_001_1366,
      avif_1440: avif_001_1440,
      avif_1920: avif_001_1920,
      avif_2560: avif_001_2560,
      avif_3840: avif_001_3840,
      webp_480: webp_001_480,
      webp_640: webp_001_640,
      webp_720: webp_001_720,
      webp_768: webp_001_768,
      webp_1024: webp_001_1024,
      webp_1366: webp_001_1366,
      webp_1440: webp_001_1440,
      webp_1920: webp_001_1920,
      webp_2560: webp_001_2560,
      webp_3840: webp_001_3840
    },
    description: 'Outback',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/800',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_002_480,
      avif_640: avif_002_640,
      avif_720: avif_002_720,
      avif_768: avif_002_768,
      avif_1024: avif_002_1024,
      avif_1366: avif_002_1366,
      avif_1440: avif_002_1440,
      avif_1920: avif_002_1920,
      avif_2560: avif_002_2560,
      avif_3840: avif_002_3840,
      webp_480: webp_002_480,
      webp_640: webp_002_640,
      webp_720: webp_002_720,
      webp_768: webp_002_768,
      webp_1024: webp_002_1024,
      webp_1366: webp_002_1366,
      webp_1440: webp_002_1440,
      webp_1920: webp_002_1920,
      webp_2560: webp_002_2560,
      webp_3840: webp_002_3840
    },
    description: 'Watarrka: flies',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/1.8',
    shutterSpeed: '1/1000',
    iso: 160
  },
  {
    urls: {
      avif_480: avif_003_480,
      avif_640: avif_003_640,
      avif_720: avif_003_720,
      avif_768: avif_003_768,
      avif_1024: avif_003_1024,
      avif_1366: avif_003_1366,
      avif_1440: avif_003_1440,
      avif_1920: avif_003_1920,
      avif_2560: avif_003_2560,
      avif_3840: avif_003_3840,
      webp_480: webp_003_480,
      webp_640: webp_003_640,
      webp_720: webp_003_720,
      webp_768: webp_003_768,
      webp_1024: webp_003_1024,
      webp_1366: webp_003_1366,
      webp_1440: webp_003_1440,
      webp_1920: webp_003_1920,
      webp_2560: webp_003_2560,
      webp_3840: webp_003_3840
    },
    description: 'Watarrka',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/100',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_004_480,
      avif_640: avif_004_640,
      avif_720: avif_004_720,
      avif_768: avif_004_768,
      avif_1024: avif_004_1024,
      avif_1366: avif_004_1366,
      avif_1440: avif_004_1440,
      avif_1920: avif_004_1920,
      avif_2560: avif_004_2560,
      avif_3840: avif_004_3840,
      webp_480: webp_004_480,
      webp_640: webp_004_640,
      webp_720: webp_004_720,
      webp_768: webp_004_768,
      webp_1024: webp_004_1024,
      webp_1366: webp_004_1366,
      webp_1440: webp_004_1440,
      webp_1920: webp_004_1920,
      webp_2560: webp_004_2560,
      webp_3840: webp_004_3840
    },
    description: 'Watarrka: lizard',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/1250',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_005_480,
      avif_640: avif_005_640,
      avif_720: avif_005_720,
      avif_768: avif_005_768,
      avif_1024: avif_005_1024,
      avif_1366: avif_005_1366,
      avif_1440: avif_005_1440,
      avif_1920: avif_005_1920,
      avif_2560: avif_005_2560,
      avif_3840: avif_005_3840,
      webp_480: webp_005_480,
      webp_640: webp_005_640,
      webp_720: webp_005_720,
      webp_768: webp_005_768,
      webp_1024: webp_005_1024,
      webp_1366: webp_005_1366,
      webp_1440: webp_005_1440,
      webp_1920: webp_005_1920,
      webp_2560: webp_005_2560,
      webp_3840: webp_005_3840
    },
    description: 'Watarrka: Spinifex Pigeon',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/2000',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_006_480,
      avif_640: avif_006_640,
      avif_720: avif_006_720,
      avif_768: avif_006_768,
      avif_1024: avif_006_1024,
      avif_1366: avif_006_1366,
      avif_1440: avif_006_1440,
      avif_1920: avif_006_1920,
      avif_2560: avif_006_2560,
      avif_3840: avif_006_3840,
      webp_480: webp_006_480,
      webp_640: webp_006_640,
      webp_720: webp_006_720,
      webp_768: webp_006_768,
      webp_1024: webp_006_1024,
      webp_1366: webp_006_1366,
      webp_1440: webp_006_1440,
      webp_1920: webp_006_1920,
      webp_2560: webp_006_2560,
      webp_3840: webp_006_3840
    },
    description: 'Kata Tjuta',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/5.0',
    shutterSpeed: '1/1250',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_007_480,
      avif_640: avif_007_640,
      avif_720: avif_007_720,
      avif_768: avif_007_768,
      avif_1024: avif_007_1024,
      avif_1366: avif_007_1366,
      avif_1440: avif_007_1440,
      avif_1920: avif_007_1920,
      avif_2560: avif_007_2560,
      avif_3840: avif_007_3840,
      webp_480: webp_007_480,
      webp_640: webp_007_640,
      webp_720: webp_007_720,
      webp_768: webp_007_768,
      webp_1024: webp_007_1024,
      webp_1366: webp_007_1366,
      webp_1440: webp_007_1440,
      webp_1920: webp_007_1920,
      webp_2560: webp_007_2560,
      webp_3840: webp_007_3840
    },
    description: 'Outback: storm & wildfire',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/6.3',
    shutterSpeed: '1/1250',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_008_480,
      avif_640: avif_008_640,
      avif_720: avif_008_720,
      avif_768: avif_008_768,
      avif_1024: avif_008_1024,
      avif_1366: avif_008_1366,
      avif_1440: avif_008_1440,
      avif_1920: avif_008_1920,
      avif_2560: avif_008_2560,
      avif_3840: avif_008_3840,
      webp_480: webp_008_480,
      webp_640: webp_008_640,
      webp_720: webp_008_720,
      webp_768: webp_008_768,
      webp_1024: webp_008_1024,
      webp_1366: webp_008_1366,
      webp_1440: webp_008_1440,
      webp_1920: webp_008_1920,
      webp_2560: webp_008_2560,
      webp_3840: webp_008_3840
    },
    description: 'Kata Tjuta: storm & wildfire',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/500',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_009_480,
      avif_640: avif_009_640,
      avif_720: avif_009_720,
      avif_768: avif_009_768,
      avif_1024: avif_009_1024,
      avif_1366: avif_009_1366,
      avif_1440: avif_009_1440,
      avif_1920: avif_009_1920,
      avif_2560: avif_009_2560,
      avif_3840: avif_009_3840,
      webp_480: webp_009_480,
      webp_640: webp_009_640,
      webp_720: webp_009_720,
      webp_768: webp_009_768,
      webp_1024: webp_009_1024,
      webp_1366: webp_009_1366,
      webp_1440: webp_009_1440,
      webp_1920: webp_009_1920,
      webp_2560: webp_009_2560,
      webp_3840: webp_009_3840
    },
    description: 'Uluru: sunset',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/100',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_010_480,
      avif_640: avif_010_640,
      avif_720: avif_010_720,
      avif_768: avif_010_768,
      avif_1024: avif_010_1024,
      avif_1366: avif_010_1366,
      avif_1440: avif_010_1440,
      avif_1920: avif_010_1920,
      avif_2560: avif_010_2560,
      avif_3840: avif_010_3840,
      webp_480: webp_010_480,
      webp_640: webp_010_640,
      webp_720: webp_010_720,
      webp_768: webp_010_768,
      webp_1024: webp_010_1024,
      webp_1366: webp_010_1366,
      webp_1440: webp_010_1440,
      webp_1920: webp_010_1920,
      webp_2560: webp_010_2560,
      webp_3840: webp_010_3840
    },
    description: 'Kata Tjuta: sunset',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/160',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_011_480,
      avif_640: avif_011_640,
      avif_720: avif_011_720,
      avif_768: avif_011_768,
      avif_1024: avif_011_1024,
      avif_1366: avif_011_1366,
      avif_1440: avif_011_1440,
      avif_1920: avif_011_1920,
      avif_2560: avif_011_2560,
      avif_3840: avif_011_3840,
      webp_480: webp_011_480,
      webp_640: webp_011_640,
      webp_720: webp_011_720,
      webp_768: webp_011_768,
      webp_1024: webp_011_1024,
      webp_1366: webp_011_1366,
      webp_1440: webp_011_1440,
      webp_1920: webp_011_1920,
      webp_2560: webp_011_2560,
      webp_3840: webp_011_3840
    },
    description: 'Coober Pedy: sunset',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/3.2',
    shutterSpeed: '1/30',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_012_480,
      avif_640: avif_012_640,
      avif_720: avif_012_720,
      avif_768: avif_012_768,
      avif_1024: avif_012_1024,
      avif_1366: avif_012_1366,
      avif_1440: avif_012_1440,
      avif_1920: avif_012_1920,
      avif_2560: avif_012_2560,
      avif_3840: avif_012_3840,
      webp_480: webp_012_480,
      webp_640: webp_012_640,
      webp_720: webp_012_720,
      webp_768: webp_012_768,
      webp_1024: webp_012_1024,
      webp_1366: webp_012_1366,
      webp_1440: webp_012_1440,
      webp_1920: webp_012_1920,
      webp_2560: webp_012_2560,
      webp_3840: webp_012_3840
    },
    description: 'Blue Mountains: lizard',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/2.8',
    shutterSpeed: '1/1250',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_013_480,
      avif_640: avif_013_640,
      avif_720: avif_013_720,
      avif_768: avif_013_768,
      avif_1024: avif_013_1024,
      avif_1366: avif_013_1366,
      avif_1440: avif_013_1440,
      avif_1920: avif_013_1920,
      avif_2560: avif_013_2560,
      avif_3840: avif_013_3840,
      webp_480: webp_013_480,
      webp_640: webp_013_640,
      webp_720: webp_013_720,
      webp_768: webp_013_768,
      webp_1024: webp_013_1024,
      webp_1366: webp_013_1366,
      webp_1440: webp_013_1440,
      webp_1920: webp_013_1920,
      webp_2560: webp_013_2560,
      webp_3840: webp_013_3840
    },
    description: 'Blue Mountains',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/1000',
    iso: 125
  }
];
