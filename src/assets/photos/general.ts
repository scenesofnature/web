import {cameras, Image, lenses} from '../../utils/image.ts';

import avif_about_1024 from './generated/general/about-1024.avif';
import webp_about_1024 from './generated/general/about-1024.webp';
import avif_about_1366 from './generated/general/about-1366.avif';
import webp_about_1366 from './generated/general/about-1366.webp';
import avif_about_1440 from './generated/general/about-1440.avif';
import webp_about_1440 from './generated/general/about-1440.webp';
import avif_about_1920 from './generated/general/about-1920.avif';
import webp_about_1920 from './generated/general/about-1920.webp';
import avif_about_2560 from './generated/general/about-2560.avif';
import webp_about_2560 from './generated/general/about-2560.webp';
import avif_about_3840 from './generated/general/about-3840.avif';
import webp_about_3840 from './generated/general/about-3840.webp';
import avif_about_480 from './generated/general/about-480.avif';
import webp_about_480 from './generated/general/about-480.webp';
import avif_about_640 from './generated/general/about-640.avif';
import webp_about_640 from './generated/general/about-640.webp';
import avif_about_720 from './generated/general/about-720.avif';
import webp_about_720 from './generated/general/about-720.webp';
import avif_about_768 from './generated/general/about-768.avif';
import webp_about_768 from './generated/general/about-768.webp';

import avif_landing_1024 from './generated/general/landing-1024.avif';
import webp_landing_1024 from './generated/general/landing-1024.webp';
import avif_landing_1366 from './generated/general/landing-1366.avif';
import webp_landing_1366 from './generated/general/landing-1366.webp';
import avif_landing_1440 from './generated/general/landing-1440.avif';
import webp_landing_1440 from './generated/general/landing-1440.webp';
import avif_landing_1920 from './generated/general/landing-1920.avif';
import webp_landing_1920 from './generated/general/landing-1920.webp';
import avif_landing_2560 from './generated/general/landing-2560.avif';
import webp_landing_2560 from './generated/general/landing-2560.webp';
import avif_landing_3840 from './generated/general/landing-3840.avif';
import webp_landing_3840 from './generated/general/landing-3840.webp';
import avif_landing_480 from './generated/general/landing-480.avif';
import webp_landing_480 from './generated/general/landing-480.webp';
import avif_landing_640 from './generated/general/landing-640.avif';
import webp_landing_640 from './generated/general/landing-640.webp';
import avif_landing_720 from './generated/general/landing-720.avif';
import webp_landing_720 from './generated/general/landing-720.webp';
import avif_landing_768 from './generated/general/landing-768.avif';
import webp_landing_768 from './generated/general/landing-768.webp';

import avif_portrait_1024 from './generated/general/portrait-1024.avif';
import webp_portrait_1024 from './generated/general/portrait-1024.webp';
import avif_portrait_1366 from './generated/general/portrait-1366.avif';
import webp_portrait_1366 from './generated/general/portrait-1366.webp';
import avif_portrait_1440 from './generated/general/portrait-1440.avif';
import webp_portrait_1440 from './generated/general/portrait-1440.webp';
import avif_portrait_1920 from './generated/general/portrait-1920.avif';
import webp_portrait_1920 from './generated/general/portrait-1920.webp';
import avif_portrait_2560 from './generated/general/portrait-2560.avif';
import webp_portrait_2560 from './generated/general/portrait-2560.webp';
import avif_portrait_3840 from './generated/general/portrait-3840.avif';
import webp_portrait_3840 from './generated/general/portrait-3840.webp';
import avif_portrait_480 from './generated/general/portrait-480.avif';
import webp_portrait_480 from './generated/general/portrait-480.webp';
import avif_portrait_640 from './generated/general/portrait-640.avif';
import webp_portrait_640 from './generated/general/portrait-640.webp';
import avif_portrait_720 from './generated/general/portrait-720.avif';
import webp_portrait_720 from './generated/general/portrait-720.webp';
import avif_portrait_768 from './generated/general/portrait-768.avif';
import webp_portrait_768 from './generated/general/portrait-768.webp';

import avif_portraitLandscape_1024 from './generated/general/portrait-landscape-1024.avif';
import webp_portraitLandscape_1024 from './generated/general/portrait-landscape-1024.webp';
import avif_portraitLandscape_1366 from './generated/general/portrait-landscape-1366.avif';
import webp_portraitLandscape_1366 from './generated/general/portrait-landscape-1366.webp';
import avif_portraitLandscape_1440 from './generated/general/portrait-landscape-1440.avif';
import webp_portraitLandscape_1440 from './generated/general/portrait-landscape-1440.webp';
import avif_portraitLandscape_1920 from './generated/general/portrait-landscape-1920.avif';
import webp_portraitLandscape_1920 from './generated/general/portrait-landscape-1920.webp';
import avif_portraitLandscape_2560 from './generated/general/portrait-landscape-2560.avif';
import webp_portraitLandscape_2560 from './generated/general/portrait-landscape-2560.webp';
import avif_portraitLandscape_3840 from './generated/general/portrait-landscape-3840.avif';
import webp_portraitLandscape_3840 from './generated/general/portrait-landscape-3840.webp';
import avif_portraitLandscape_480 from './generated/general/portrait-landscape-480.avif';
import webp_portraitLandscape_480 from './generated/general/portrait-landscape-480.webp';
import avif_portraitLandscape_640 from './generated/general/portrait-landscape-640.avif';
import webp_portraitLandscape_640 from './generated/general/portrait-landscape-640.webp';
import avif_portraitLandscape_720 from './generated/general/portrait-landscape-720.avif';
import webp_portraitLandscape_720 from './generated/general/portrait-landscape-720.webp';
import avif_portraitLandscape_768 from './generated/general/portrait-landscape-768.avif';
import webp_portraitLandscape_768 from './generated/general/portrait-landscape-768.webp';

export const landingImage: Image = {
  urls: {
    avif_480: avif_landing_480,
    avif_640: avif_landing_640,
    avif_720: avif_landing_720,
    avif_768: avif_landing_768,
    avif_1024: avif_landing_1024,
    avif_1366: avif_landing_1366,
    avif_1440: avif_landing_1440,
    avif_1920: avif_landing_1920,
    avif_2560: avif_landing_2560,
    avif_3840: avif_landing_3840,
    webp_480: webp_landing_480,
    webp_640: webp_landing_640,
    webp_720: webp_landing_720,
    webp_768: webp_landing_768,
    webp_1024: webp_landing_1024,
    webp_1366: webp_landing_1366,
    webp_1440: webp_landing_1440,
    webp_1920: webp_landing_1920,
    webp_2560: webp_landing_2560,
    webp_3840: webp_landing_3840
  },
  description: 'Piopiotahi: Stirling Falls',
  camera: cameras.powerShotG7Xii,
  aperture: 'f/4.0',
  shutterSpeed: '1/320',
  iso: 125
};

export const portraitImage: Image = {
  urls: {
    avif_480: avif_portrait_480,
    avif_640: avif_portrait_640,
    avif_720: avif_portrait_720,
    avif_768: avif_portrait_768,
    avif_1024: avif_portrait_1024,
    avif_1366: avif_portrait_1366,
    avif_1440: avif_portrait_1440,
    avif_1920: avif_portrait_1920,
    avif_2560: avif_portrait_2560,
    avif_3840: avif_portrait_3840,
    webp_480: webp_portrait_480,
    webp_640: webp_portrait_640,
    webp_720: webp_portrait_720,
    webp_768: webp_portrait_768,
    webp_1024: webp_portrait_1024,
    webp_1366: webp_portrait_1366,
    webp_1440: webp_portrait_1440,
    webp_1920: webp_portrait_1920,
    webp_2560: webp_portrait_2560,
    webp_3840: webp_portrait_3840
  },
  description: 'Simon Jadwiczek',
  camera: cameras.r6ii,
  lens: lenses.rf70_200,
  aperture: 'f/2.8',
  shutterSpeed: '1/400',
  iso: 100
};

export const portraitLandscapeImage: Image = {
  urls: {
    avif_480: avif_portraitLandscape_480,
    avif_640: avif_portraitLandscape_640,
    avif_720: avif_portraitLandscape_720,
    avif_768: avif_portraitLandscape_768,
    avif_1024: avif_portraitLandscape_1024,
    avif_1366: avif_portraitLandscape_1366,
    avif_1440: avif_portraitLandscape_1440,
    avif_1920: avif_portraitLandscape_1920,
    avif_2560: avif_portraitLandscape_2560,
    avif_3840: avif_portraitLandscape_3840,
    webp_480: webp_portraitLandscape_480,
    webp_640: webp_portraitLandscape_640,
    webp_720: webp_portraitLandscape_720,
    webp_768: webp_portraitLandscape_768,
    webp_1024: webp_portraitLandscape_1024,
    webp_1366: webp_portraitLandscape_1366,
    webp_1440: webp_portraitLandscape_1440,
    webp_1920: webp_portraitLandscape_1920,
    webp_2560: webp_portraitLandscape_2560,
    webp_3840: webp_portraitLandscape_3840
  },
  description: 'Simon Jadwiczek',
  camera: cameras.r6ii,
  lens: lenses.rf70_200,
  aperture: 'f/2.8',
  shutterSpeed: '1/400',
  iso: 100
};

export const aboutImage: Image = {
  urls: {
    avif_480: avif_about_480,
    avif_640: avif_about_640,
    avif_720: avif_about_720,
    avif_768: avif_about_768,
    avif_1024: avif_about_1024,
    avif_1366: avif_about_1366,
    avif_1440: avif_about_1440,
    avif_1920: avif_about_1920,
    avif_2560: avif_about_2560,
    avif_3840: avif_about_3840,
    webp_480: webp_about_480,
    webp_640: webp_about_640,
    webp_720: webp_about_720,
    webp_768: webp_about_768,
    webp_1024: webp_about_1024,
    webp_1366: webp_about_1366,
    webp_1440: webp_about_1440,
    webp_1920: webp_about_1920,
    webp_2560: webp_about_2560,
    webp_3840: webp_about_3840
  },
  description: 'photograph by MANUEL DIEẞNER\nNepal, way to Ama Dablam Base Camp',
  camera: cameras.powerShotG7Xii,
  aperture: 'f/5.0',
  shutterSpeed: '1/1250',
  iso: 125
};
