import {cameras, Image, lenses} from '../../utils/image.ts';

import avif_001_1024 from './generated/006-birds/001-1024.avif';
import webp_001_1024 from './generated/006-birds/001-1024.webp';
import avif_001_1366 from './generated/006-birds/001-1366.avif';
import webp_001_1366 from './generated/006-birds/001-1366.webp';
import avif_001_1440 from './generated/006-birds/001-1440.avif';
import webp_001_1440 from './generated/006-birds/001-1440.webp';
import avif_001_1920 from './generated/006-birds/001-1920.avif';
import webp_001_1920 from './generated/006-birds/001-1920.webp';
import avif_001_2560 from './generated/006-birds/001-2560.avif';
import webp_001_2560 from './generated/006-birds/001-2560.webp';
import avif_001_3840 from './generated/006-birds/001-3840.avif';
import webp_001_3840 from './generated/006-birds/001-3840.webp';
import avif_001_480 from './generated/006-birds/001-480.avif';
import webp_001_480 from './generated/006-birds/001-480.webp';
import avif_001_640 from './generated/006-birds/001-640.avif';
import webp_001_640 from './generated/006-birds/001-640.webp';
import avif_001_720 from './generated/006-birds/001-720.avif';
import webp_001_720 from './generated/006-birds/001-720.webp';
import avif_001_768 from './generated/006-birds/001-768.avif';
import webp_001_768 from './generated/006-birds/001-768.webp';

import avif_002_1024 from './generated/006-birds/002-1024.avif';
import webp_002_1024 from './generated/006-birds/002-1024.webp';
import avif_002_1366 from './generated/006-birds/002-1366.avif';
import webp_002_1366 from './generated/006-birds/002-1366.webp';
import avif_002_1440 from './generated/006-birds/002-1440.avif';
import webp_002_1440 from './generated/006-birds/002-1440.webp';
import avif_002_1920 from './generated/006-birds/002-1920.avif';
import webp_002_1920 from './generated/006-birds/002-1920.webp';
import avif_002_2560 from './generated/006-birds/002-2560.avif';
import webp_002_2560 from './generated/006-birds/002-2560.webp';
import avif_002_3840 from './generated/006-birds/002-3840.avif';
import webp_002_3840 from './generated/006-birds/002-3840.webp';
import avif_002_480 from './generated/006-birds/002-480.avif';
import webp_002_480 from './generated/006-birds/002-480.webp';
import avif_002_640 from './generated/006-birds/002-640.avif';
import webp_002_640 from './generated/006-birds/002-640.webp';
import avif_002_720 from './generated/006-birds/002-720.avif';
import webp_002_720 from './generated/006-birds/002-720.webp';
import avif_002_768 from './generated/006-birds/002-768.avif';
import webp_002_768 from './generated/006-birds/002-768.webp';

import avif_003_1024 from './generated/006-birds/003-1024.avif';
import webp_003_1024 from './generated/006-birds/003-1024.webp';
import avif_003_1366 from './generated/006-birds/003-1366.avif';
import webp_003_1366 from './generated/006-birds/003-1366.webp';
import avif_003_1440 from './generated/006-birds/003-1440.avif';
import webp_003_1440 from './generated/006-birds/003-1440.webp';
import avif_003_1920 from './generated/006-birds/003-1920.avif';
import webp_003_1920 from './generated/006-birds/003-1920.webp';
import avif_003_2560 from './generated/006-birds/003-2560.avif';
import webp_003_2560 from './generated/006-birds/003-2560.webp';
import avif_003_3840 from './generated/006-birds/003-3840.avif';
import webp_003_3840 from './generated/006-birds/003-3840.webp';
import avif_003_480 from './generated/006-birds/003-480.avif';
import webp_003_480 from './generated/006-birds/003-480.webp';
import avif_003_640 from './generated/006-birds/003-640.avif';
import webp_003_640 from './generated/006-birds/003-640.webp';
import avif_003_720 from './generated/006-birds/003-720.avif';
import webp_003_720 from './generated/006-birds/003-720.webp';
import avif_003_768 from './generated/006-birds/003-768.avif';
import webp_003_768 from './generated/006-birds/003-768.webp';

import avif_004_1024 from './generated/006-birds/004-1024.avif';
import webp_004_1024 from './generated/006-birds/004-1024.webp';
import avif_004_1366 from './generated/006-birds/004-1366.avif';
import webp_004_1366 from './generated/006-birds/004-1366.webp';
import avif_004_1440 from './generated/006-birds/004-1440.avif';
import webp_004_1440 from './generated/006-birds/004-1440.webp';
import avif_004_1920 from './generated/006-birds/004-1920.avif';
import webp_004_1920 from './generated/006-birds/004-1920.webp';
import avif_004_2560 from './generated/006-birds/004-2560.avif';
import webp_004_2560 from './generated/006-birds/004-2560.webp';
import avif_004_3840 from './generated/006-birds/004-3840.avif';
import webp_004_3840 from './generated/006-birds/004-3840.webp';
import avif_004_480 from './generated/006-birds/004-480.avif';
import webp_004_480 from './generated/006-birds/004-480.webp';
import avif_004_640 from './generated/006-birds/004-640.avif';
import webp_004_640 from './generated/006-birds/004-640.webp';
import avif_004_720 from './generated/006-birds/004-720.avif';
import webp_004_720 from './generated/006-birds/004-720.webp';
import avif_004_768 from './generated/006-birds/004-768.avif';
import webp_004_768 from './generated/006-birds/004-768.webp';

import avif_005_1024 from './generated/006-birds/005-1024.avif';
import webp_005_1024 from './generated/006-birds/005-1024.webp';
import avif_005_1366 from './generated/006-birds/005-1366.avif';
import webp_005_1366 from './generated/006-birds/005-1366.webp';
import avif_005_1440 from './generated/006-birds/005-1440.avif';
import webp_005_1440 from './generated/006-birds/005-1440.webp';
import avif_005_1920 from './generated/006-birds/005-1920.avif';
import webp_005_1920 from './generated/006-birds/005-1920.webp';
import avif_005_2560 from './generated/006-birds/005-2560.avif';
import webp_005_2560 from './generated/006-birds/005-2560.webp';
import avif_005_3840 from './generated/006-birds/005-3840.avif';
import webp_005_3840 from './generated/006-birds/005-3840.webp';
import avif_005_480 from './generated/006-birds/005-480.avif';
import webp_005_480 from './generated/006-birds/005-480.webp';
import avif_005_640 from './generated/006-birds/005-640.avif';
import webp_005_640 from './generated/006-birds/005-640.webp';
import avif_005_720 from './generated/006-birds/005-720.avif';
import webp_005_720 from './generated/006-birds/005-720.webp';
import avif_005_768 from './generated/006-birds/005-768.avif';
import webp_005_768 from './generated/006-birds/005-768.webp';

import avif_006_1024 from './generated/006-birds/006-1024.avif';
import webp_006_1024 from './generated/006-birds/006-1024.webp';
import avif_006_1366 from './generated/006-birds/006-1366.avif';
import webp_006_1366 from './generated/006-birds/006-1366.webp';
import avif_006_1440 from './generated/006-birds/006-1440.avif';
import webp_006_1440 from './generated/006-birds/006-1440.webp';
import avif_006_1920 from './generated/006-birds/006-1920.avif';
import webp_006_1920 from './generated/006-birds/006-1920.webp';
import avif_006_2560 from './generated/006-birds/006-2560.avif';
import webp_006_2560 from './generated/006-birds/006-2560.webp';
import avif_006_3840 from './generated/006-birds/006-3840.avif';
import webp_006_3840 from './generated/006-birds/006-3840.webp';
import avif_006_480 from './generated/006-birds/006-480.avif';
import webp_006_480 from './generated/006-birds/006-480.webp';
import avif_006_640 from './generated/006-birds/006-640.avif';
import webp_006_640 from './generated/006-birds/006-640.webp';
import avif_006_720 from './generated/006-birds/006-720.avif';
import webp_006_720 from './generated/006-birds/006-720.webp';
import avif_006_768 from './generated/006-birds/006-768.avif';
import webp_006_768 from './generated/006-birds/006-768.webp';

import avif_007_1024 from './generated/006-birds/007-1024.avif';
import webp_007_1024 from './generated/006-birds/007-1024.webp';
import avif_007_1366 from './generated/006-birds/007-1366.avif';
import webp_007_1366 from './generated/006-birds/007-1366.webp';
import avif_007_1440 from './generated/006-birds/007-1440.avif';
import webp_007_1440 from './generated/006-birds/007-1440.webp';
import avif_007_1920 from './generated/006-birds/007-1920.avif';
import webp_007_1920 from './generated/006-birds/007-1920.webp';
import avif_007_2560 from './generated/006-birds/007-2560.avif';
import webp_007_2560 from './generated/006-birds/007-2560.webp';
import avif_007_3840 from './generated/006-birds/007-3840.avif';
import webp_007_3840 from './generated/006-birds/007-3840.webp';
import avif_007_480 from './generated/006-birds/007-480.avif';
import webp_007_480 from './generated/006-birds/007-480.webp';
import avif_007_640 from './generated/006-birds/007-640.avif';
import webp_007_640 from './generated/006-birds/007-640.webp';
import avif_007_720 from './generated/006-birds/007-720.avif';
import webp_007_720 from './generated/006-birds/007-720.webp';
import avif_007_768 from './generated/006-birds/007-768.avif';
import webp_007_768 from './generated/006-birds/007-768.webp';

import avif_008_1024 from './generated/006-birds/008-1024.avif';
import webp_008_1024 from './generated/006-birds/008-1024.webp';
import avif_008_1366 from './generated/006-birds/008-1366.avif';
import webp_008_1366 from './generated/006-birds/008-1366.webp';
import avif_008_1440 from './generated/006-birds/008-1440.avif';
import webp_008_1440 from './generated/006-birds/008-1440.webp';
import avif_008_1920 from './generated/006-birds/008-1920.avif';
import webp_008_1920 from './generated/006-birds/008-1920.webp';
import avif_008_2560 from './generated/006-birds/008-2560.avif';
import webp_008_2560 from './generated/006-birds/008-2560.webp';
import avif_008_3840 from './generated/006-birds/008-3840.avif';
import webp_008_3840 from './generated/006-birds/008-3840.webp';
import avif_008_480 from './generated/006-birds/008-480.avif';
import webp_008_480 from './generated/006-birds/008-480.webp';
import avif_008_640 from './generated/006-birds/008-640.avif';
import webp_008_640 from './generated/006-birds/008-640.webp';
import avif_008_720 from './generated/006-birds/008-720.avif';
import webp_008_720 from './generated/006-birds/008-720.webp';
import avif_008_768 from './generated/006-birds/008-768.avif';
import webp_008_768 from './generated/006-birds/008-768.webp';

import avif_009_1024 from './generated/006-birds/009-1024.avif';
import webp_009_1024 from './generated/006-birds/009-1024.webp';
import avif_009_1366 from './generated/006-birds/009-1366.avif';
import webp_009_1366 from './generated/006-birds/009-1366.webp';
import avif_009_1440 from './generated/006-birds/009-1440.avif';
import webp_009_1440 from './generated/006-birds/009-1440.webp';
import avif_009_1920 from './generated/006-birds/009-1920.avif';
import webp_009_1920 from './generated/006-birds/009-1920.webp';
import avif_009_2560 from './generated/006-birds/009-2560.avif';
import webp_009_2560 from './generated/006-birds/009-2560.webp';
import avif_009_3840 from './generated/006-birds/009-3840.avif';
import webp_009_3840 from './generated/006-birds/009-3840.webp';
import avif_009_480 from './generated/006-birds/009-480.avif';
import webp_009_480 from './generated/006-birds/009-480.webp';
import avif_009_640 from './generated/006-birds/009-640.avif';
import webp_009_640 from './generated/006-birds/009-640.webp';
import avif_009_720 from './generated/006-birds/009-720.avif';
import webp_009_720 from './generated/006-birds/009-720.webp';
import avif_009_768 from './generated/006-birds/009-768.avif';
import webp_009_768 from './generated/006-birds/009-768.webp';

import avif_010_1024 from './generated/006-birds/010-1024.avif';
import webp_010_1024 from './generated/006-birds/010-1024.webp';
import avif_010_1366 from './generated/006-birds/010-1366.avif';
import webp_010_1366 from './generated/006-birds/010-1366.webp';
import avif_010_1440 from './generated/006-birds/010-1440.avif';
import webp_010_1440 from './generated/006-birds/010-1440.webp';
import avif_010_1920 from './generated/006-birds/010-1920.avif';
import webp_010_1920 from './generated/006-birds/010-1920.webp';
import avif_010_2560 from './generated/006-birds/010-2560.avif';
import webp_010_2560 from './generated/006-birds/010-2560.webp';
import avif_010_3840 from './generated/006-birds/010-3840.avif';
import webp_010_3840 from './generated/006-birds/010-3840.webp';
import avif_010_480 from './generated/006-birds/010-480.avif';
import webp_010_480 from './generated/006-birds/010-480.webp';
import avif_010_640 from './generated/006-birds/010-640.avif';
import webp_010_640 from './generated/006-birds/010-640.webp';
import avif_010_720 from './generated/006-birds/010-720.avif';
import webp_010_720 from './generated/006-birds/010-720.webp';
import avif_010_768 from './generated/006-birds/010-768.avif';
import webp_010_768 from './generated/006-birds/010-768.webp';

import avif_011_1024 from './generated/006-birds/011-1024.avif';
import webp_011_1024 from './generated/006-birds/011-1024.webp';
import avif_011_1366 from './generated/006-birds/011-1366.avif';
import webp_011_1366 from './generated/006-birds/011-1366.webp';
import avif_011_1440 from './generated/006-birds/011-1440.avif';
import webp_011_1440 from './generated/006-birds/011-1440.webp';
import avif_011_1920 from './generated/006-birds/011-1920.avif';
import webp_011_1920 from './generated/006-birds/011-1920.webp';
import avif_011_2560 from './generated/006-birds/011-2560.avif';
import webp_011_2560 from './generated/006-birds/011-2560.webp';
import avif_011_3840 from './generated/006-birds/011-3840.avif';
import webp_011_3840 from './generated/006-birds/011-3840.webp';
import avif_011_480 from './generated/006-birds/011-480.avif';
import webp_011_480 from './generated/006-birds/011-480.webp';
import avif_011_640 from './generated/006-birds/011-640.avif';
import webp_011_640 from './generated/006-birds/011-640.webp';
import avif_011_720 from './generated/006-birds/011-720.avif';
import webp_011_720 from './generated/006-birds/011-720.webp';
import avif_011_768 from './generated/006-birds/011-768.avif';
import webp_011_768 from './generated/006-birds/011-768.webp';

import avif_012_1024 from './generated/006-birds/012-1024.avif';
import webp_012_1024 from './generated/006-birds/012-1024.webp';
import avif_012_1366 from './generated/006-birds/012-1366.avif';
import webp_012_1366 from './generated/006-birds/012-1366.webp';
import avif_012_1440 from './generated/006-birds/012-1440.avif';
import webp_012_1440 from './generated/006-birds/012-1440.webp';
import avif_012_1920 from './generated/006-birds/012-1920.avif';
import webp_012_1920 from './generated/006-birds/012-1920.webp';
import avif_012_2560 from './generated/006-birds/012-2560.avif';
import webp_012_2560 from './generated/006-birds/012-2560.webp';
import avif_012_3840 from './generated/006-birds/012-3840.avif';
import webp_012_3840 from './generated/006-birds/012-3840.webp';
import avif_012_480 from './generated/006-birds/012-480.avif';
import webp_012_480 from './generated/006-birds/012-480.webp';
import avif_012_640 from './generated/006-birds/012-640.avif';
import webp_012_640 from './generated/006-birds/012-640.webp';
import avif_012_720 from './generated/006-birds/012-720.avif';
import webp_012_720 from './generated/006-birds/012-720.webp';
import avif_012_768 from './generated/006-birds/012-768.avif';
import webp_012_768 from './generated/006-birds/012-768.webp';

import avif_013_1024 from './generated/006-birds/013-1024.avif';
import webp_013_1024 from './generated/006-birds/013-1024.webp';
import avif_013_1366 from './generated/006-birds/013-1366.avif';
import webp_013_1366 from './generated/006-birds/013-1366.webp';
import avif_013_1440 from './generated/006-birds/013-1440.avif';
import webp_013_1440 from './generated/006-birds/013-1440.webp';
import avif_013_1920 from './generated/006-birds/013-1920.avif';
import webp_013_1920 from './generated/006-birds/013-1920.webp';
import avif_013_2560 from './generated/006-birds/013-2560.avif';
import webp_013_2560 from './generated/006-birds/013-2560.webp';
import avif_013_3840 from './generated/006-birds/013-3840.avif';
import webp_013_3840 from './generated/006-birds/013-3840.webp';
import avif_013_480 from './generated/006-birds/013-480.avif';
import webp_013_480 from './generated/006-birds/013-480.webp';
import avif_013_640 from './generated/006-birds/013-640.avif';
import webp_013_640 from './generated/006-birds/013-640.webp';
import avif_013_720 from './generated/006-birds/013-720.avif';
import webp_013_720 from './generated/006-birds/013-720.webp';
import avif_013_768 from './generated/006-birds/013-768.avif';
import webp_013_768 from './generated/006-birds/013-768.webp';

import avif_014_1024 from './generated/006-birds/014-1024.avif';
import webp_014_1024 from './generated/006-birds/014-1024.webp';
import avif_014_1366 from './generated/006-birds/014-1366.avif';
import webp_014_1366 from './generated/006-birds/014-1366.webp';
import avif_014_1440 from './generated/006-birds/014-1440.avif';
import webp_014_1440 from './generated/006-birds/014-1440.webp';
import avif_014_1920 from './generated/006-birds/014-1920.avif';
import webp_014_1920 from './generated/006-birds/014-1920.webp';
import avif_014_2560 from './generated/006-birds/014-2560.avif';
import webp_014_2560 from './generated/006-birds/014-2560.webp';
import avif_014_3840 from './generated/006-birds/014-3840.avif';
import webp_014_3840 from './generated/006-birds/014-3840.webp';
import avif_014_480 from './generated/006-birds/014-480.avif';
import webp_014_480 from './generated/006-birds/014-480.webp';
import avif_014_640 from './generated/006-birds/014-640.avif';
import webp_014_640 from './generated/006-birds/014-640.webp';
import avif_014_720 from './generated/006-birds/014-720.avif';
import webp_014_720 from './generated/006-birds/014-720.webp';
import avif_014_768 from './generated/006-birds/014-768.avif';
import webp_014_768 from './generated/006-birds/014-768.webp';

import avif_015_1024 from './generated/006-birds/015-1024.avif';
import webp_015_1024 from './generated/006-birds/015-1024.webp';
import avif_015_1366 from './generated/006-birds/015-1366.avif';
import webp_015_1366 from './generated/006-birds/015-1366.webp';
import avif_015_1440 from './generated/006-birds/015-1440.avif';
import webp_015_1440 from './generated/006-birds/015-1440.webp';
import avif_015_1920 from './generated/006-birds/015-1920.avif';
import webp_015_1920 from './generated/006-birds/015-1920.webp';
import avif_015_2560 from './generated/006-birds/015-2560.avif';
import webp_015_2560 from './generated/006-birds/015-2560.webp';
import avif_015_3840 from './generated/006-birds/015-3840.avif';
import webp_015_3840 from './generated/006-birds/015-3840.webp';
import avif_015_480 from './generated/006-birds/015-480.avif';
import webp_015_480 from './generated/006-birds/015-480.webp';
import avif_015_640 from './generated/006-birds/015-640.avif';
import webp_015_640 from './generated/006-birds/015-640.webp';
import avif_015_720 from './generated/006-birds/015-720.avif';
import webp_015_720 from './generated/006-birds/015-720.webp';
import avif_015_768 from './generated/006-birds/015-768.avif';
import webp_015_768 from './generated/006-birds/015-768.webp';

import avif_016_1024 from './generated/006-birds/016-1024.avif';
import webp_016_1024 from './generated/006-birds/016-1024.webp';
import avif_016_1366 from './generated/006-birds/016-1366.avif';
import webp_016_1366 from './generated/006-birds/016-1366.webp';
import avif_016_1440 from './generated/006-birds/016-1440.avif';
import webp_016_1440 from './generated/006-birds/016-1440.webp';
import avif_016_1920 from './generated/006-birds/016-1920.avif';
import webp_016_1920 from './generated/006-birds/016-1920.webp';
import avif_016_2560 from './generated/006-birds/016-2560.avif';
import webp_016_2560 from './generated/006-birds/016-2560.webp';
import avif_016_3840 from './generated/006-birds/016-3840.avif';
import webp_016_3840 from './generated/006-birds/016-3840.webp';
import avif_016_480 from './generated/006-birds/016-480.avif';
import webp_016_480 from './generated/006-birds/016-480.webp';
import avif_016_640 from './generated/006-birds/016-640.avif';
import webp_016_640 from './generated/006-birds/016-640.webp';
import avif_016_720 from './generated/006-birds/016-720.avif';
import webp_016_720 from './generated/006-birds/016-720.webp';
import avif_016_768 from './generated/006-birds/016-768.avif';
import webp_016_768 from './generated/006-birds/016-768.webp';

import avif_017_1024 from './generated/006-birds/017-1024.avif';
import webp_017_1024 from './generated/006-birds/017-1024.webp';
import avif_017_1366 from './generated/006-birds/017-1366.avif';
import webp_017_1366 from './generated/006-birds/017-1366.webp';
import avif_017_1440 from './generated/006-birds/017-1440.avif';
import webp_017_1440 from './generated/006-birds/017-1440.webp';
import avif_017_1920 from './generated/006-birds/017-1920.avif';
import webp_017_1920 from './generated/006-birds/017-1920.webp';
import avif_017_2560 from './generated/006-birds/017-2560.avif';
import webp_017_2560 from './generated/006-birds/017-2560.webp';
import avif_017_3840 from './generated/006-birds/017-3840.avif';
import webp_017_3840 from './generated/006-birds/017-3840.webp';
import avif_017_480 from './generated/006-birds/017-480.avif';
import webp_017_480 from './generated/006-birds/017-480.webp';
import avif_017_640 from './generated/006-birds/017-640.avif';
import webp_017_640 from './generated/006-birds/017-640.webp';
import avif_017_720 from './generated/006-birds/017-720.avif';
import webp_017_720 from './generated/006-birds/017-720.webp';
import avif_017_768 from './generated/006-birds/017-768.avif';
import webp_017_768 from './generated/006-birds/017-768.webp';

import avif_018_1024 from './generated/006-birds/018-1024.avif';
import webp_018_1024 from './generated/006-birds/018-1024.webp';
import avif_018_1366 from './generated/006-birds/018-1366.avif';
import webp_018_1366 from './generated/006-birds/018-1366.webp';
import avif_018_1440 from './generated/006-birds/018-1440.avif';
import webp_018_1440 from './generated/006-birds/018-1440.webp';
import avif_018_1920 from './generated/006-birds/018-1920.avif';
import webp_018_1920 from './generated/006-birds/018-1920.webp';
import avif_018_2560 from './generated/006-birds/018-2560.avif';
import webp_018_2560 from './generated/006-birds/018-2560.webp';
import avif_018_3840 from './generated/006-birds/018-3840.avif';
import webp_018_3840 from './generated/006-birds/018-3840.webp';
import avif_018_480 from './generated/006-birds/018-480.avif';
import webp_018_480 from './generated/006-birds/018-480.webp';
import avif_018_640 from './generated/006-birds/018-640.avif';
import webp_018_640 from './generated/006-birds/018-640.webp';
import avif_018_720 from './generated/006-birds/018-720.avif';
import webp_018_720 from './generated/006-birds/018-720.webp';
import avif_018_768 from './generated/006-birds/018-768.avif';
import webp_018_768 from './generated/006-birds/018-768.webp';

import avif_019_1024 from './generated/006-birds/019-1024.avif';
import webp_019_1024 from './generated/006-birds/019-1024.webp';
import avif_019_1366 from './generated/006-birds/019-1366.avif';
import webp_019_1366 from './generated/006-birds/019-1366.webp';
import avif_019_1440 from './generated/006-birds/019-1440.avif';
import webp_019_1440 from './generated/006-birds/019-1440.webp';
import avif_019_1920 from './generated/006-birds/019-1920.avif';
import webp_019_1920 from './generated/006-birds/019-1920.webp';
import avif_019_2560 from './generated/006-birds/019-2560.avif';
import webp_019_2560 from './generated/006-birds/019-2560.webp';
import avif_019_3840 from './generated/006-birds/019-3840.avif';
import webp_019_3840 from './generated/006-birds/019-3840.webp';
import avif_019_480 from './generated/006-birds/019-480.avif';
import webp_019_480 from './generated/006-birds/019-480.webp';
import avif_019_640 from './generated/006-birds/019-640.avif';
import webp_019_640 from './generated/006-birds/019-640.webp';
import avif_019_720 from './generated/006-birds/019-720.avif';
import webp_019_720 from './generated/006-birds/019-720.webp';
import avif_019_768 from './generated/006-birds/019-768.avif';
import webp_019_768 from './generated/006-birds/019-768.webp';

export const birdImages: Image[] = [
  {
    urls: {
      avif_480: avif_001_480,
      avif_640: avif_001_640,
      avif_720: avif_001_720,
      avif_768: avif_001_768,
      avif_1024: avif_001_1024,
      avif_1366: avif_001_1366,
      avif_1440: avif_001_1440,
      avif_1920: avif_001_1920,
      avif_2560: avif_001_2560,
      avif_3840: avif_001_3840,
      webp_480: webp_001_480,
      webp_640: webp_001_640,
      webp_720: webp_001_720,
      webp_768: webp_001_768,
      webp_1024: webp_001_1024,
      webp_1366: webp_001_1366,
      webp_1440: webp_001_1440,
      webp_1920: webp_001_1920,
      webp_2560: webp_001_2560,
      webp_3840: webp_001_3840
    },
    description: 'Common Blackbird',
    camera: cameras.r6ii,
    lens: lenses.rf70_200,
    aperture: 'f/2.8',
    shutterSpeed: '1/100',
    iso: 12800
  },
  {
    urls: {
      avif_480: avif_002_480,
      avif_640: avif_002_640,
      avif_720: avif_002_720,
      avif_768: avif_002_768,
      avif_1024: avif_002_1024,
      avif_1366: avif_002_1366,
      avif_1440: avif_002_1440,
      avif_1920: avif_002_1920,
      avif_2560: avif_002_2560,
      avif_3840: avif_002_3840,
      webp_480: webp_002_480,
      webp_640: webp_002_640,
      webp_720: webp_002_720,
      webp_768: webp_002_768,
      webp_1024: webp_002_1024,
      webp_1366: webp_002_1366,
      webp_1440: webp_002_1440,
      webp_1920: webp_002_1920,
      webp_2560: webp_002_2560,
      webp_3840: webp_002_3840
    },
    description: 'Mute Swan',
    camera: cameras.r6ii,
    lens: lenses.rf70_200,
    aperture: 'f/2.8',
    shutterSpeed: '1/1000',
    iso: 250
  },
  {
    urls: {
      avif_480: avif_003_480,
      avif_640: avif_003_640,
      avif_720: avif_003_720,
      avif_768: avif_003_768,
      avif_1024: avif_003_1024,
      avif_1366: avif_003_1366,
      avif_1440: avif_003_1440,
      avif_1920: avif_003_1920,
      avif_2560: avif_003_2560,
      avif_3840: avif_003_3840,
      webp_480: webp_003_480,
      webp_640: webp_003_640,
      webp_720: webp_003_720,
      webp_768: webp_003_768,
      webp_1024: webp_003_1024,
      webp_1366: webp_003_1366,
      webp_1440: webp_003_1440,
      webp_1920: webp_003_1920,
      webp_2560: webp_003_2560,
      webp_3840: webp_003_3840
    },
    description: 'Eurasian Blue Tit',
    camera: cameras.r6ii,
    lens: lenses.rf70_200,
    aperture: 'f/2.8',
    shutterSpeed: '1/1000',
    iso: 160
  },
  {
    urls: {
      avif_480: avif_004_480,
      avif_640: avif_004_640,
      avif_720: avif_004_720,
      avif_768: avif_004_768,
      avif_1024: avif_004_1024,
      avif_1366: avif_004_1366,
      avif_1440: avif_004_1440,
      avif_1920: avif_004_1920,
      avif_2560: avif_004_2560,
      avif_3840: avif_004_3840,
      webp_480: webp_004_480,
      webp_640: webp_004_640,
      webp_720: webp_004_720,
      webp_768: webp_004_768,
      webp_1024: webp_004_1024,
      webp_1366: webp_004_1366,
      webp_1440: webp_004_1440,
      webp_1920: webp_004_1920,
      webp_2560: webp_004_2560,
      webp_3840: webp_004_3840
    },
    description: 'Eurasian Blue Tit',
    camera: cameras.r6ii,
    lens: lenses.rf70_200,
    aperture: 'f/2.8',
    shutterSpeed: '1/1000',
    iso: 250
  },
  {
    urls: {
      avif_480: avif_005_480,
      avif_640: avif_005_640,
      avif_720: avif_005_720,
      avif_768: avif_005_768,
      avif_1024: avif_005_1024,
      avif_1366: avif_005_1366,
      avif_1440: avif_005_1440,
      avif_1920: avif_005_1920,
      avif_2560: avif_005_2560,
      avif_3840: avif_005_3840,
      webp_480: webp_005_480,
      webp_640: webp_005_640,
      webp_720: webp_005_720,
      webp_768: webp_005_768,
      webp_1024: webp_005_1024,
      webp_1366: webp_005_1366,
      webp_1440: webp_005_1440,
      webp_1920: webp_005_1920,
      webp_2560: webp_005_2560,
      webp_3840: webp_005_3840
    },
    description: 'Common Goldeneye',
    camera: cameras.r6ii,
    lens: lenses.rf70_200,
    aperture: 'f/2.8',
    shutterSpeed: '1/4000',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_006_480,
      avif_640: avif_006_640,
      avif_720: avif_006_720,
      avif_768: avif_006_768,
      avif_1024: avif_006_1024,
      avif_1366: avif_006_1366,
      avif_1440: avif_006_1440,
      avif_1920: avif_006_1920,
      avif_2560: avif_006_2560,
      avif_3840: avif_006_3840,
      webp_480: webp_006_480,
      webp_640: webp_006_640,
      webp_720: webp_006_720,
      webp_768: webp_006_768,
      webp_1024: webp_006_1024,
      webp_1366: webp_006_1366,
      webp_1440: webp_006_1440,
      webp_1920: webp_006_1920,
      webp_2560: webp_006_2560,
      webp_3840: webp_006_3840
    },
    description: 'White Wagtail',
    camera: cameras.r6ii,
    lens: lenses.rf70_200,
    aperture: 'f/3.2',
    shutterSpeed: '1/5000',
    iso: 200
  },
  {
    urls: {
      avif_480: avif_007_480,
      avif_640: avif_007_640,
      avif_720: avif_007_720,
      avif_768: avif_007_768,
      avif_1024: avif_007_1024,
      avif_1366: avif_007_1366,
      avif_1440: avif_007_1440,
      avif_1920: avif_007_1920,
      avif_2560: avif_007_2560,
      avif_3840: avif_007_3840,
      webp_480: webp_007_480,
      webp_640: webp_007_640,
      webp_720: webp_007_720,
      webp_768: webp_007_768,
      webp_1024: webp_007_1024,
      webp_1366: webp_007_1366,
      webp_1440: webp_007_1440,
      webp_1920: webp_007_1920,
      webp_2560: webp_007_2560,
      webp_3840: webp_007_3840
    },
    description: 'Bramblings',
    camera: cameras.r6ii,
    lens: lenses.ef300,
    aperture: 'f/2.8',
    shutterSpeed: '1/2000',
    iso: 2000
  },
  {
    urls: {
      avif_480: avif_008_480,
      avif_640: avif_008_640,
      avif_720: avif_008_720,
      avif_768: avif_008_768,
      avif_1024: avif_008_1024,
      avif_1366: avif_008_1366,
      avif_1440: avif_008_1440,
      avif_1920: avif_008_1920,
      avif_2560: avif_008_2560,
      avif_3840: avif_008_3840,
      webp_480: webp_008_480,
      webp_640: webp_008_640,
      webp_720: webp_008_720,
      webp_768: webp_008_768,
      webp_1024: webp_008_1024,
      webp_1366: webp_008_1366,
      webp_1440: webp_008_1440,
      webp_1920: webp_008_1920,
      webp_2560: webp_008_2560,
      webp_3840: webp_008_3840
    },
    description: 'House Sparrow',
    camera: cameras.r6ii,
    lens: lenses.rf70_200,
    aperture: 'f/2.8',
    shutterSpeed: '1/800',
    iso: 1000
  },
  {
    urls: {
      avif_480: avif_009_480,
      avif_640: avif_009_640,
      avif_720: avif_009_720,
      avif_768: avif_009_768,
      avif_1024: avif_009_1024,
      avif_1366: avif_009_1366,
      avif_1440: avif_009_1440,
      avif_1920: avif_009_1920,
      avif_2560: avif_009_2560,
      avif_3840: avif_009_3840,
      webp_480: webp_009_480,
      webp_640: webp_009_640,
      webp_720: webp_009_720,
      webp_768: webp_009_768,
      webp_1024: webp_009_1024,
      webp_1366: webp_009_1366,
      webp_1440: webp_009_1440,
      webp_1920: webp_009_1920,
      webp_2560: webp_009_2560,
      webp_3840: webp_009_3840
    },
    description: 'Eurasian Blue Tits',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.0',
    shutterSpeed: '1/1600',
    iso: 3200
  },
  {
    urls: {
      avif_480: avif_010_480,
      avif_640: avif_010_640,
      avif_720: avif_010_720,
      avif_768: avif_010_768,
      avif_1024: avif_010_1024,
      avif_1366: avif_010_1366,
      avif_1440: avif_010_1440,
      avif_1920: avif_010_1920,
      avif_2560: avif_010_2560,
      avif_3840: avif_010_3840,
      webp_480: webp_010_480,
      webp_640: webp_010_640,
      webp_720: webp_010_720,
      webp_768: webp_010_768,
      webp_1024: webp_010_1024,
      webp_1366: webp_010_1366,
      webp_1440: webp_010_1440,
      webp_1920: webp_010_1920,
      webp_2560: webp_010_2560,
      webp_3840: webp_010_3840
    },
    description: 'House Sparrow',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/5.6',
    shutterSpeed: '1/1000',
    iso: 1600
  },
  {
    urls: {
      avif_480: avif_011_480,
      avif_640: avif_011_640,
      avif_720: avif_011_720,
      avif_768: avif_011_768,
      avif_1024: avif_011_1024,
      avif_1366: avif_011_1366,
      avif_1440: avif_011_1440,
      avif_1920: avif_011_1920,
      avif_2560: avif_011_2560,
      avif_3840: avif_011_3840,
      webp_480: webp_011_480,
      webp_640: webp_011_640,
      webp_720: webp_011_720,
      webp_768: webp_011_768,
      webp_1024: webp_011_1024,
      webp_1366: webp_011_1366,
      webp_1440: webp_011_1440,
      webp_1920: webp_011_1920,
      webp_2560: webp_011_2560,
      webp_3840: webp_011_3840
    },
    description: 'Eurasian Wren',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.5',
    shutterSpeed: '1/1250',
    iso: 2000
  },
  {
    urls: {
      avif_480: avif_012_480,
      avif_640: avif_012_640,
      avif_720: avif_012_720,
      avif_768: avif_012_768,
      avif_1024: avif_012_1024,
      avif_1366: avif_012_1366,
      avif_1440: avif_012_1440,
      avif_1920: avif_012_1920,
      avif_2560: avif_012_2560,
      avif_3840: avif_012_3840,
      webp_480: webp_012_480,
      webp_640: webp_012_640,
      webp_720: webp_012_720,
      webp_768: webp_012_768,
      webp_1024: webp_012_1024,
      webp_1366: webp_012_1366,
      webp_1440: webp_012_1440,
      webp_1920: webp_012_1920,
      webp_2560: webp_012_2560,
      webp_3840: webp_012_3840
    },
    description: 'Great Tit',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.0',
    shutterSpeed: '1/2000',
    iso: 1600
  },
  {
    urls: {
      avif_480: avif_013_480,
      avif_640: avif_013_640,
      avif_720: avif_013_720,
      avif_768: avif_013_768,
      avif_1024: avif_013_1024,
      avif_1366: avif_013_1366,
      avif_1440: avif_013_1440,
      avif_1920: avif_013_1920,
      avif_2560: avif_013_2560,
      avif_3840: avif_013_3840,
      webp_480: webp_013_480,
      webp_640: webp_013_640,
      webp_720: webp_013_720,
      webp_768: webp_013_768,
      webp_1024: webp_013_1024,
      webp_1366: webp_013_1366,
      webp_1440: webp_013_1440,
      webp_1920: webp_013_1920,
      webp_2560: webp_013_2560,
      webp_3840: webp_013_3840
    },
    description: 'European Bee-Eaters',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.5',
    shutterSpeed: '1/1250',
    iso: 160
  },
  {
    urls: {
      avif_480: avif_014_480,
      avif_640: avif_014_640,
      avif_720: avif_014_720,
      avif_768: avif_014_768,
      avif_1024: avif_014_1024,
      avif_1366: avif_014_1366,
      avif_1440: avif_014_1440,
      avif_1920: avif_014_1920,
      avif_2560: avif_014_2560,
      avif_3840: avif_014_3840,
      webp_480: webp_014_480,
      webp_640: webp_014_640,
      webp_720: webp_014_720,
      webp_768: webp_014_768,
      webp_1024: webp_014_1024,
      webp_1366: webp_014_1366,
      webp_1440: webp_014_1440,
      webp_1920: webp_014_1920,
      webp_2560: webp_014_2560,
      webp_3840: webp_014_3840
    },
    description: 'European Bee-Eater',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.0',
    shutterSpeed: '1/640',
    iso: 100
  },
  {
    urls: {
      avif_480: avif_015_480,
      avif_640: avif_015_640,
      avif_720: avif_015_720,
      avif_768: avif_015_768,
      avif_1024: avif_015_1024,
      avif_1366: avif_015_1366,
      avif_1440: avif_015_1440,
      avif_1920: avif_015_1920,
      avif_2560: avif_015_2560,
      avif_3840: avif_015_3840,
      webp_480: webp_015_480,
      webp_640: webp_015_640,
      webp_720: webp_015_720,
      webp_768: webp_015_768,
      webp_1024: webp_015_1024,
      webp_1366: webp_015_1366,
      webp_1440: webp_015_1440,
      webp_1920: webp_015_1920,
      webp_2560: webp_015_2560,
      webp_3840: webp_015_3840
    },
    description: 'European Bee-Eater',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.0',
    shutterSpeed: '1/2000',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_016_480,
      avif_640: avif_016_640,
      avif_720: avif_016_720,
      avif_768: avif_016_768,
      avif_1024: avif_016_1024,
      avif_1366: avif_016_1366,
      avif_1440: avif_016_1440,
      avif_1920: avif_016_1920,
      avif_2560: avif_016_2560,
      avif_3840: avif_016_3840,
      webp_480: webp_016_480,
      webp_640: webp_016_640,
      webp_720: webp_016_720,
      webp_768: webp_016_768,
      webp_1024: webp_016_1024,
      webp_1366: webp_016_1366,
      webp_1440: webp_016_1440,
      webp_1920: webp_016_1920,
      webp_2560: webp_016_2560,
      webp_3840: webp_016_3840
    },
    description: 'European Bee-Eater',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.0',
    shutterSpeed: '1/1600',
    iso: 640
  },
  {
    urls: {
      avif_480: avif_017_480,
      avif_640: avif_017_640,
      avif_720: avif_017_720,
      avif_768: avif_017_768,
      avif_1024: avif_017_1024,
      avif_1366: avif_017_1366,
      avif_1440: avif_017_1440,
      avif_1920: avif_017_1920,
      avif_2560: avif_017_2560,
      avif_3840: avif_017_3840,
      webp_480: webp_017_480,
      webp_640: webp_017_640,
      webp_720: webp_017_720,
      webp_768: webp_017_768,
      webp_1024: webp_017_1024,
      webp_1366: webp_017_1366,
      webp_1440: webp_017_1440,
      webp_1920: webp_017_1920,
      webp_2560: webp_017_2560,
      webp_3840: webp_017_3840
    },
    description: 'European Bee-Eaters',
    camera: cameras.r6ii,
    lens: lenses.ef500,
    aperture: 'f/4.5',
    shutterSpeed: '1/1250',
    iso: 250
  },
  {
    urls: {
      avif_480: avif_018_480,
      avif_640: avif_018_640,
      avif_720: avif_018_720,
      avif_768: avif_018_768,
      avif_1024: avif_018_1024,
      avif_1366: avif_018_1366,
      avif_1440: avif_018_1440,
      avif_1920: avif_018_1920,
      avif_2560: avif_018_2560,
      avif_3840: avif_018_3840,
      webp_480: webp_018_480,
      webp_640: webp_018_640,
      webp_720: webp_018_720,
      webp_768: webp_018_768,
      webp_1024: webp_018_1024,
      webp_1366: webp_018_1366,
      webp_1440: webp_018_1440,
      webp_1920: webp_018_1920,
      webp_2560: webp_018_2560,
      webp_3840: webp_018_3840
    },
    description: 'African Penguin',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/2.8',
    shutterSpeed: '1/100',
    iso: 200
  },
  {
    urls: {
      avif_480: avif_019_480,
      avif_640: avif_019_640,
      avif_720: avif_019_720,
      avif_768: avif_019_768,
      avif_1024: avif_019_1024,
      avif_1366: avif_019_1366,
      avif_1440: avif_019_1440,
      avif_1920: avif_019_1920,
      avif_2560: avif_019_2560,
      avif_3840: avif_019_3840,
      webp_480: webp_019_480,
      webp_640: webp_019_640,
      webp_720: webp_019_720,
      webp_768: webp_019_768,
      webp_1024: webp_019_1024,
      webp_1366: webp_019_1366,
      webp_1440: webp_019_1440,
      webp_1920: webp_019_1920,
      webp_2560: webp_019_2560,
      webp_3840: webp_019_3840
    },
    description: 'African Penguin',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/3.2',
    shutterSpeed: '1/100',
    iso: 125
  }
];
