import {cameras, Image} from '../../utils/image.ts';

import avif_001_1024 from './generated/003-new-zealand/001-1024.avif';
import webp_001_1024 from './generated/003-new-zealand/001-1024.webp';
import avif_001_1366 from './generated/003-new-zealand/001-1366.avif';
import webp_001_1366 from './generated/003-new-zealand/001-1366.webp';
import avif_001_1440 from './generated/003-new-zealand/001-1440.avif';
import webp_001_1440 from './generated/003-new-zealand/001-1440.webp';
import avif_001_1920 from './generated/003-new-zealand/001-1920.avif';
import webp_001_1920 from './generated/003-new-zealand/001-1920.webp';
import avif_001_2560 from './generated/003-new-zealand/001-2560.avif';
import webp_001_2560 from './generated/003-new-zealand/001-2560.webp';
import avif_001_3840 from './generated/003-new-zealand/001-3840.avif';
import webp_001_3840 from './generated/003-new-zealand/001-3840.webp';
import avif_001_480 from './generated/003-new-zealand/001-480.avif';
import webp_001_480 from './generated/003-new-zealand/001-480.webp';
import avif_001_640 from './generated/003-new-zealand/001-640.avif';
import webp_001_640 from './generated/003-new-zealand/001-640.webp';
import avif_001_720 from './generated/003-new-zealand/001-720.avif';
import webp_001_720 from './generated/003-new-zealand/001-720.webp';
import avif_001_768 from './generated/003-new-zealand/001-768.avif';
import webp_001_768 from './generated/003-new-zealand/001-768.webp';

import avif_002_1024 from './generated/003-new-zealand/002-1024.avif';
import webp_002_1024 from './generated/003-new-zealand/002-1024.webp';
import avif_002_1366 from './generated/003-new-zealand/002-1366.avif';
import webp_002_1366 from './generated/003-new-zealand/002-1366.webp';
import avif_002_1440 from './generated/003-new-zealand/002-1440.avif';
import webp_002_1440 from './generated/003-new-zealand/002-1440.webp';
import avif_002_1920 from './generated/003-new-zealand/002-1920.avif';
import webp_002_1920 from './generated/003-new-zealand/002-1920.webp';
import avif_002_2560 from './generated/003-new-zealand/002-2560.avif';
import webp_002_2560 from './generated/003-new-zealand/002-2560.webp';
import avif_002_3840 from './generated/003-new-zealand/002-3840.avif';
import webp_002_3840 from './generated/003-new-zealand/002-3840.webp';
import avif_002_480 from './generated/003-new-zealand/002-480.avif';
import webp_002_480 from './generated/003-new-zealand/002-480.webp';
import avif_002_640 from './generated/003-new-zealand/002-640.avif';
import webp_002_640 from './generated/003-new-zealand/002-640.webp';
import avif_002_720 from './generated/003-new-zealand/002-720.avif';
import webp_002_720 from './generated/003-new-zealand/002-720.webp';
import avif_002_768 from './generated/003-new-zealand/002-768.avif';
import webp_002_768 from './generated/003-new-zealand/002-768.webp';

import avif_003_1024 from './generated/003-new-zealand/003-1024.avif';
import webp_003_1024 from './generated/003-new-zealand/003-1024.webp';
import avif_003_1366 from './generated/003-new-zealand/003-1366.avif';
import webp_003_1366 from './generated/003-new-zealand/003-1366.webp';
import avif_003_1440 from './generated/003-new-zealand/003-1440.avif';
import webp_003_1440 from './generated/003-new-zealand/003-1440.webp';
import avif_003_1920 from './generated/003-new-zealand/003-1920.avif';
import webp_003_1920 from './generated/003-new-zealand/003-1920.webp';
import avif_003_2560 from './generated/003-new-zealand/003-2560.avif';
import webp_003_2560 from './generated/003-new-zealand/003-2560.webp';
import avif_003_3840 from './generated/003-new-zealand/003-3840.avif';
import webp_003_3840 from './generated/003-new-zealand/003-3840.webp';
import avif_003_480 from './generated/003-new-zealand/003-480.avif';
import webp_003_480 from './generated/003-new-zealand/003-480.webp';
import avif_003_640 from './generated/003-new-zealand/003-640.avif';
import webp_003_640 from './generated/003-new-zealand/003-640.webp';
import avif_003_720 from './generated/003-new-zealand/003-720.avif';
import webp_003_720 from './generated/003-new-zealand/003-720.webp';
import avif_003_768 from './generated/003-new-zealand/003-768.avif';
import webp_003_768 from './generated/003-new-zealand/003-768.webp';

import avif_004_1024 from './generated/003-new-zealand/004-1024.avif';
import webp_004_1024 from './generated/003-new-zealand/004-1024.webp';
import avif_004_1366 from './generated/003-new-zealand/004-1366.avif';
import webp_004_1366 from './generated/003-new-zealand/004-1366.webp';
import avif_004_1440 from './generated/003-new-zealand/004-1440.avif';
import webp_004_1440 from './generated/003-new-zealand/004-1440.webp';
import avif_004_1920 from './generated/003-new-zealand/004-1920.avif';
import webp_004_1920 from './generated/003-new-zealand/004-1920.webp';
import avif_004_2560 from './generated/003-new-zealand/004-2560.avif';
import webp_004_2560 from './generated/003-new-zealand/004-2560.webp';
import avif_004_3840 from './generated/003-new-zealand/004-3840.avif';
import webp_004_3840 from './generated/003-new-zealand/004-3840.webp';
import avif_004_480 from './generated/003-new-zealand/004-480.avif';
import webp_004_480 from './generated/003-new-zealand/004-480.webp';
import avif_004_640 from './generated/003-new-zealand/004-640.avif';
import webp_004_640 from './generated/003-new-zealand/004-640.webp';
import avif_004_720 from './generated/003-new-zealand/004-720.avif';
import webp_004_720 from './generated/003-new-zealand/004-720.webp';
import avif_004_768 from './generated/003-new-zealand/004-768.avif';
import webp_004_768 from './generated/003-new-zealand/004-768.webp';

import avif_005_1024 from './generated/003-new-zealand/005-1024.avif';
import webp_005_1024 from './generated/003-new-zealand/005-1024.webp';
import avif_005_1366 from './generated/003-new-zealand/005-1366.avif';
import webp_005_1366 from './generated/003-new-zealand/005-1366.webp';
import avif_005_1440 from './generated/003-new-zealand/005-1440.avif';
import webp_005_1440 from './generated/003-new-zealand/005-1440.webp';
import avif_005_1920 from './generated/003-new-zealand/005-1920.avif';
import webp_005_1920 from './generated/003-new-zealand/005-1920.webp';
import avif_005_2560 from './generated/003-new-zealand/005-2560.avif';
import webp_005_2560 from './generated/003-new-zealand/005-2560.webp';
import avif_005_3840 from './generated/003-new-zealand/005-3840.avif';
import webp_005_3840 from './generated/003-new-zealand/005-3840.webp';
import avif_005_480 from './generated/003-new-zealand/005-480.avif';
import webp_005_480 from './generated/003-new-zealand/005-480.webp';
import avif_005_640 from './generated/003-new-zealand/005-640.avif';
import webp_005_640 from './generated/003-new-zealand/005-640.webp';
import avif_005_720 from './generated/003-new-zealand/005-720.avif';
import webp_005_720 from './generated/003-new-zealand/005-720.webp';
import avif_005_768 from './generated/003-new-zealand/005-768.avif';
import webp_005_768 from './generated/003-new-zealand/005-768.webp';

import avif_006_1024 from './generated/003-new-zealand/006-1024.avif';
import webp_006_1024 from './generated/003-new-zealand/006-1024.webp';
import avif_006_1366 from './generated/003-new-zealand/006-1366.avif';
import webp_006_1366 from './generated/003-new-zealand/006-1366.webp';
import avif_006_1440 from './generated/003-new-zealand/006-1440.avif';
import webp_006_1440 from './generated/003-new-zealand/006-1440.webp';
import avif_006_1920 from './generated/003-new-zealand/006-1920.avif';
import webp_006_1920 from './generated/003-new-zealand/006-1920.webp';
import avif_006_2560 from './generated/003-new-zealand/006-2560.avif';
import webp_006_2560 from './generated/003-new-zealand/006-2560.webp';
import avif_006_3840 from './generated/003-new-zealand/006-3840.avif';
import webp_006_3840 from './generated/003-new-zealand/006-3840.webp';
import avif_006_480 from './generated/003-new-zealand/006-480.avif';
import webp_006_480 from './generated/003-new-zealand/006-480.webp';
import avif_006_640 from './generated/003-new-zealand/006-640.avif';
import webp_006_640 from './generated/003-new-zealand/006-640.webp';
import avif_006_720 from './generated/003-new-zealand/006-720.avif';
import webp_006_720 from './generated/003-new-zealand/006-720.webp';
import avif_006_768 from './generated/003-new-zealand/006-768.avif';
import webp_006_768 from './generated/003-new-zealand/006-768.webp';

import avif_007_1024 from './generated/003-new-zealand/007-1024.avif';
import webp_007_1024 from './generated/003-new-zealand/007-1024.webp';
import avif_007_1366 from './generated/003-new-zealand/007-1366.avif';
import webp_007_1366 from './generated/003-new-zealand/007-1366.webp';
import avif_007_1440 from './generated/003-new-zealand/007-1440.avif';
import webp_007_1440 from './generated/003-new-zealand/007-1440.webp';
import avif_007_1920 from './generated/003-new-zealand/007-1920.avif';
import webp_007_1920 from './generated/003-new-zealand/007-1920.webp';
import avif_007_2560 from './generated/003-new-zealand/007-2560.avif';
import webp_007_2560 from './generated/003-new-zealand/007-2560.webp';
import avif_007_3840 from './generated/003-new-zealand/007-3840.avif';
import webp_007_3840 from './generated/003-new-zealand/007-3840.webp';
import avif_007_480 from './generated/003-new-zealand/007-480.avif';
import webp_007_480 from './generated/003-new-zealand/007-480.webp';
import avif_007_640 from './generated/003-new-zealand/007-640.avif';
import webp_007_640 from './generated/003-new-zealand/007-640.webp';
import avif_007_720 from './generated/003-new-zealand/007-720.avif';
import webp_007_720 from './generated/003-new-zealand/007-720.webp';
import avif_007_768 from './generated/003-new-zealand/007-768.avif';
import webp_007_768 from './generated/003-new-zealand/007-768.webp';

import avif_008_1024 from './generated/003-new-zealand/008-1024.avif';
import webp_008_1024 from './generated/003-new-zealand/008-1024.webp';
import avif_008_1366 from './generated/003-new-zealand/008-1366.avif';
import webp_008_1366 from './generated/003-new-zealand/008-1366.webp';
import avif_008_1440 from './generated/003-new-zealand/008-1440.avif';
import webp_008_1440 from './generated/003-new-zealand/008-1440.webp';
import avif_008_1920 from './generated/003-new-zealand/008-1920.avif';
import webp_008_1920 from './generated/003-new-zealand/008-1920.webp';
import avif_008_2560 from './generated/003-new-zealand/008-2560.avif';
import webp_008_2560 from './generated/003-new-zealand/008-2560.webp';
import avif_008_3840 from './generated/003-new-zealand/008-3840.avif';
import webp_008_3840 from './generated/003-new-zealand/008-3840.webp';
import avif_008_480 from './generated/003-new-zealand/008-480.avif';
import webp_008_480 from './generated/003-new-zealand/008-480.webp';
import avif_008_640 from './generated/003-new-zealand/008-640.avif';
import webp_008_640 from './generated/003-new-zealand/008-640.webp';
import avif_008_720 from './generated/003-new-zealand/008-720.avif';
import webp_008_720 from './generated/003-new-zealand/008-720.webp';
import avif_008_768 from './generated/003-new-zealand/008-768.avif';
import webp_008_768 from './generated/003-new-zealand/008-768.webp';

import avif_009_1024 from './generated/003-new-zealand/009-1024.avif';
import webp_009_1024 from './generated/003-new-zealand/009-1024.webp';
import avif_009_1366 from './generated/003-new-zealand/009-1366.avif';
import webp_009_1366 from './generated/003-new-zealand/009-1366.webp';
import avif_009_1440 from './generated/003-new-zealand/009-1440.avif';
import webp_009_1440 from './generated/003-new-zealand/009-1440.webp';
import avif_009_1920 from './generated/003-new-zealand/009-1920.avif';
import webp_009_1920 from './generated/003-new-zealand/009-1920.webp';
import avif_009_2560 from './generated/003-new-zealand/009-2560.avif';
import webp_009_2560 from './generated/003-new-zealand/009-2560.webp';
import avif_009_3840 from './generated/003-new-zealand/009-3840.avif';
import webp_009_3840 from './generated/003-new-zealand/009-3840.webp';
import avif_009_480 from './generated/003-new-zealand/009-480.avif';
import webp_009_480 from './generated/003-new-zealand/009-480.webp';
import avif_009_640 from './generated/003-new-zealand/009-640.avif';
import webp_009_640 from './generated/003-new-zealand/009-640.webp';
import avif_009_720 from './generated/003-new-zealand/009-720.avif';
import webp_009_720 from './generated/003-new-zealand/009-720.webp';
import avif_009_768 from './generated/003-new-zealand/009-768.avif';
import webp_009_768 from './generated/003-new-zealand/009-768.webp';

import avif_010_1024 from './generated/003-new-zealand/010-1024.avif';
import webp_010_1024 from './generated/003-new-zealand/010-1024.webp';
import avif_010_1366 from './generated/003-new-zealand/010-1366.avif';
import webp_010_1366 from './generated/003-new-zealand/010-1366.webp';
import avif_010_1440 from './generated/003-new-zealand/010-1440.avif';
import webp_010_1440 from './generated/003-new-zealand/010-1440.webp';
import avif_010_1920 from './generated/003-new-zealand/010-1920.avif';
import webp_010_1920 from './generated/003-new-zealand/010-1920.webp';
import avif_010_2560 from './generated/003-new-zealand/010-2560.avif';
import webp_010_2560 from './generated/003-new-zealand/010-2560.webp';
import avif_010_3840 from './generated/003-new-zealand/010-3840.avif';
import webp_010_3840 from './generated/003-new-zealand/010-3840.webp';
import avif_010_480 from './generated/003-new-zealand/010-480.avif';
import webp_010_480 from './generated/003-new-zealand/010-480.webp';
import avif_010_640 from './generated/003-new-zealand/010-640.avif';
import webp_010_640 from './generated/003-new-zealand/010-640.webp';
import avif_010_720 from './generated/003-new-zealand/010-720.avif';
import webp_010_720 from './generated/003-new-zealand/010-720.webp';
import avif_010_768 from './generated/003-new-zealand/010-768.avif';
import webp_010_768 from './generated/003-new-zealand/010-768.webp';

import avif_011_1024 from './generated/003-new-zealand/011-1024.avif';
import webp_011_1024 from './generated/003-new-zealand/011-1024.webp';
import avif_011_1366 from './generated/003-new-zealand/011-1366.avif';
import webp_011_1366 from './generated/003-new-zealand/011-1366.webp';
import avif_011_1440 from './generated/003-new-zealand/011-1440.avif';
import webp_011_1440 from './generated/003-new-zealand/011-1440.webp';
import avif_011_1920 from './generated/003-new-zealand/011-1920.avif';
import webp_011_1920 from './generated/003-new-zealand/011-1920.webp';
import avif_011_2560 from './generated/003-new-zealand/011-2560.avif';
import webp_011_2560 from './generated/003-new-zealand/011-2560.webp';
import avif_011_3840 from './generated/003-new-zealand/011-3840.avif';
import webp_011_3840 from './generated/003-new-zealand/011-3840.webp';
import avif_011_480 from './generated/003-new-zealand/011-480.avif';
import webp_011_480 from './generated/003-new-zealand/011-480.webp';
import avif_011_640 from './generated/003-new-zealand/011-640.avif';
import webp_011_640 from './generated/003-new-zealand/011-640.webp';
import avif_011_720 from './generated/003-new-zealand/011-720.avif';
import webp_011_720 from './generated/003-new-zealand/011-720.webp';
import avif_011_768 from './generated/003-new-zealand/011-768.avif';
import webp_011_768 from './generated/003-new-zealand/011-768.webp';

export const newZealandImages: Image[] = [
  {
    urls: {
      avif_480: avif_001_480,
      avif_640: avif_001_640,
      avif_720: avif_001_720,
      avif_768: avif_001_768,
      avif_1024: avif_001_1024,
      avif_1366: avif_001_1366,
      avif_1440: avif_001_1440,
      avif_1920: avif_001_1920,
      avif_2560: avif_001_2560,
      avif_3840: avif_001_3840,
      webp_480: webp_001_480,
      webp_640: webp_001_640,
      webp_720: webp_001_720,
      webp_768: webp_001_768,
      webp_1024: webp_001_1024,
      webp_1366: webp_001_1366,
      webp_1440: webp_001_1440,
      webp_1920: webp_001_1920,
      webp_2560: webp_001_2560,
      webp_3840: webp_001_3840
    },
    description: 'Kaikōura: Dusky Dolphins',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/1250',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_002_480,
      avif_640: avif_002_640,
      avif_720: avif_002_720,
      avif_768: avif_002_768,
      avif_1024: avif_002_1024,
      avif_1366: avif_002_1366,
      avif_1440: avif_002_1440,
      avif_1920: avif_002_1920,
      avif_2560: avif_002_2560,
      avif_3840: avif_002_3840,
      webp_480: webp_002_480,
      webp_640: webp_002_640,
      webp_720: webp_002_720,
      webp_768: webp_002_768,
      webp_1024: webp_002_1024,
      webp_1366: webp_002_1366,
      webp_1440: webp_002_1440,
      webp_1920: webp_002_1920,
      webp_2560: webp_002_2560,
      webp_3840: webp_002_3840
    },
    description: 'Takapō',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/5.0',
    shutterSpeed: '1/1250',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_003_480,
      avif_640: avif_003_640,
      avif_720: avif_003_720,
      avif_768: avif_003_768,
      avif_1024: avif_003_1024,
      avif_1366: avif_003_1366,
      avif_1440: avif_003_1440,
      avif_1920: avif_003_1920,
      avif_2560: avif_003_2560,
      avif_3840: avif_003_3840,
      webp_480: webp_003_480,
      webp_640: webp_003_640,
      webp_720: webp_003_720,
      webp_768: webp_003_768,
      webp_1024: webp_003_1024,
      webp_1366: webp_003_1366,
      webp_1440: webp_003_1440,
      webp_1920: webp_003_1920,
      webp_2560: webp_003_2560,
      webp_3840: webp_003_3840
    },
    description: 'Tokatā',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/1.8',
    shutterSpeed: '1/80',
    iso: 320
  },
  {
    urls: {
      avif_480: avif_004_480,
      avif_640: avif_004_640,
      avif_720: avif_004_720,
      avif_768: avif_004_768,
      avif_1024: avif_004_1024,
      avif_1366: avif_004_1366,
      avif_1440: avif_004_1440,
      avif_1920: avif_004_1920,
      avif_2560: avif_004_2560,
      avif_3840: avif_004_3840,
      webp_480: webp_004_480,
      webp_640: webp_004_640,
      webp_720: webp_004_720,
      webp_768: webp_004_768,
      webp_1024: webp_004_1024,
      webp_1366: webp_004_1366,
      webp_1440: webp_004_1440,
      webp_1920: webp_004_1920,
      webp_2560: webp_004_2560,
      webp_3840: webp_004_3840
    },
    description: 'Piopiotahi/Gertrude Saddle: Kea',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/80',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_005_480,
      avif_640: avif_005_640,
      avif_720: avif_005_720,
      avif_768: avif_005_768,
      avif_1024: avif_005_1024,
      avif_1366: avif_005_1366,
      avif_1440: avif_005_1440,
      avif_1920: avif_005_1920,
      avif_2560: avif_005_2560,
      avif_3840: avif_005_3840,
      webp_480: webp_005_480,
      webp_640: webp_005_640,
      webp_720: webp_005_720,
      webp_768: webp_005_768,
      webp_1024: webp_005_1024,
      webp_1366: webp_005_1366,
      webp_1440: webp_005_1440,
      webp_1920: webp_005_1920,
      webp_2560: webp_005_2560,
      webp_3840: webp_005_3840
    },
    description: 'Piopiotahi: Stirling Falls',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/320',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_006_480,
      avif_640: avif_006_640,
      avif_720: avif_006_720,
      avif_768: avif_006_768,
      avif_1024: avif_006_1024,
      avif_1366: avif_006_1366,
      avif_1440: avif_006_1440,
      avif_1920: avif_006_1920,
      avif_2560: avif_006_2560,
      avif_3840: avif_006_3840,
      webp_480: webp_006_480,
      webp_640: webp_006_640,
      webp_720: webp_006_720,
      webp_768: webp_006_768,
      webp_1024: webp_006_1024,
      webp_1366: webp_006_1366,
      webp_1440: webp_006_1440,
      webp_1920: webp_006_1920,
      webp_2560: webp_006_2560,
      webp_3840: webp_006_3840
    },
    description: 'Piopiotahi',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/500',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_007_480,
      avif_640: avif_007_640,
      avif_720: avif_007_720,
      avif_768: avif_007_768,
      avif_1024: avif_007_1024,
      avif_1366: avif_007_1366,
      avif_1440: avif_007_1440,
      avif_1920: avif_007_1920,
      avif_2560: avif_007_2560,
      avif_3840: avif_007_3840,
      webp_480: webp_007_480,
      webp_640: webp_007_640,
      webp_720: webp_007_720,
      webp_768: webp_007_768,
      webp_1024: webp_007_1024,
      webp_1366: webp_007_1366,
      webp_1440: webp_007_1440,
      webp_1920: webp_007_1920,
      webp_2560: webp_007_2560,
      webp_3840: webp_007_3840
    },
    description: 'Piopiotahi: Kissing Turtle Rocks',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/500',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_008_480,
      avif_640: avif_008_640,
      avif_720: avif_008_720,
      avif_768: avif_008_768,
      avif_1024: avif_008_1024,
      avif_1366: avif_008_1366,
      avif_1440: avif_008_1440,
      avif_1920: avif_008_1920,
      avif_2560: avif_008_2560,
      avif_3840: avif_008_3840,
      webp_480: webp_008_480,
      webp_640: webp_008_640,
      webp_720: webp_008_720,
      webp_768: webp_008_768,
      webp_1024: webp_008_1024,
      webp_1366: webp_008_1366,
      webp_1440: webp_008_1440,
      webp_1920: webp_008_1920,
      webp_2560: webp_008_2560,
      webp_3840: webp_008_3840
    },
    description: 'Piopiotahi: Marian Cascade',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/1.8',
    shutterSpeed: '4.0',
    iso: 160
  },
  {
    urls: {
      avif_480: avif_009_480,
      avif_640: avif_009_640,
      avif_720: avif_009_720,
      avif_768: avif_009_768,
      avif_1024: avif_009_1024,
      avif_1366: avif_009_1366,
      avif_1440: avif_009_1440,
      avif_1920: avif_009_1920,
      avif_2560: avif_009_2560,
      avif_3840: avif_009_3840,
      webp_480: webp_009_480,
      webp_640: webp_009_640,
      webp_720: webp_009_720,
      webp_768: webp_009_768,
      webp_1024: webp_009_1024,
      webp_1366: webp_009_1366,
      webp_1440: webp_009_1440,
      webp_1920: webp_009_1920,
      webp_2560: webp_009_2560,
      webp_3840: webp_009_3840
    },
    description: 'Te Whaka-ata',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/3.5',
    shutterSpeed: '1/2000',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_010_480,
      avif_640: avif_010_640,
      avif_720: avif_010_720,
      avif_768: avif_010_768,
      avif_1024: avif_010_1024,
      avif_1366: avif_010_1366,
      avif_1440: avif_010_1440,
      avif_1920: avif_010_1920,
      avif_2560: avif_010_2560,
      avif_3840: avif_010_3840,
      webp_480: webp_010_480,
      webp_640: webp_010_640,
      webp_720: webp_010_720,
      webp_768: webp_010_768,
      webp_1024: webp_010_1024,
      webp_1366: webp_010_1366,
      webp_1440: webp_010_1440,
      webp_1920: webp_010_1920,
      webp_2560: webp_010_2560,
      webp_3840: webp_010_3840
    },
    description: 'Tongariro National Park/Red Crater (1886m)',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.5',
    shutterSpeed: '1/1250',
    iso: 125
  },
  {
    urls: {
      avif_480: avif_011_480,
      avif_640: avif_011_640,
      avif_720: avif_011_720,
      avif_768: avif_011_768,
      avif_1024: avif_011_1024,
      avif_1366: avif_011_1366,
      avif_1440: avif_011_1440,
      avif_1920: avif_011_1920,
      avif_2560: avif_011_2560,
      avif_3840: avif_011_3840,
      webp_480: webp_011_480,
      webp_640: webp_011_640,
      webp_720: webp_011_720,
      webp_768: webp_011_768,
      webp_1024: webp_011_1024,
      webp_1366: webp_011_1366,
      webp_1440: webp_011_1440,
      webp_1920: webp_011_1920,
      webp_2560: webp_011_2560,
      webp_3840: webp_011_3840
    },
    description: 'Taranaki Maunga/Lake Mangamahoe',
    camera: cameras.powerShotG7Xii,
    aperture: 'f/4.0',
    shutterSpeed: '1/640',
    iso: 125
  }
];
